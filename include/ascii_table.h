/** \defgroup ascii_table ASCII Table
 * Pretty-prints an ASCII Table
 *
 * This is an add-on for the subsystem module useful for 
 * viewing the character encoding of the terminal you are 
 * working with.
 *
 * Valid commands are:
 * - $ascii help (show list of commands and info)
 * - $ascii table hex (show ascii table with hex codes)
 * - $ascii table decimal (show ascii table with decimal codes)
 * - $ascii table dec (same as above)
 *
 * \author Josh Klodnicki
 *
 * Created on March 8, 2016, 12:41 AM
 *
 * \todo Document this module
 * @{
 */

#ifndef _ASCII_TABLE_H_
#define	_ASCII_TABLE_H_

/** Initialize the module (register a module "ascii" with a callback)
 * 
 */
void ascii_init(void);

#endif	/* ASCII_TABLE_H */

///@}
