#ifndef _UART_PACKET_H_
#define _UART_PACKET_H_
/**
 * 
 * @defgroup uart_packet UART Packet Transport Module
 * 
 * Make sure to use the USE_UPACKET_n flags in system.h so the module knows
 * which UPACKET buffers to allocate (where n is the UART channel number)
 * 
 * Packets are transmitted in the following format:
 * 
 * [start low][start high][length low][length high][packet data bytes][checksum low][checksum high]
 * 
 * The length (in bytes) may be up to UPACKET_MAX_LENGTH (default is 512) bytes
 * long. UPACKET_MAX_LENGTH may be overriden by defining it in system.h.
 * 
 * The checksum is calculated as ~(length word + (sum of packet bytes)) + 1 so 
 * that: length word + (sum of packet bytes) + checksum word == 0
 * 
 * Dependencies:
 * - uart.c/.h
 *      - hal_uart.c/.h
 *      - buffer.c/.h
 *      - charReceiverList.c/.h
 * 
 * @{
 */

#ifndef UPACKET_MAX_LENGTH
#define UPACKET_MAX_LENGTH 512
#endif

#define UPACKET_START_L 0xAA /**< intentionally above 0x7F so it is outside of 
the normal ASCII character range so logging and UPackets can exist on the same 
UART channel without issue */
#define UPACKET_START_H 0x55 ///< no special significance

/** @brief Initialize the UART Packet Transport Module for a particular UART
 * channel and register a callback
 * 
 * UPacket_Init will register a receiver with the specified UART module as well
 * as register the user supplied callback
 * 
 * Example:
 * @code
 * void MyCallback(uint8_t * data, uint16_t length) {
 *    // process the received packet
 * }
 * ...
 * UPacket_Init(1, MyCallback); // initialize UART Packet Transport on UART 1 and register MyCallback
 * @endcode
 * 
 * @param uart UART index to use
 * @param callback function pointer to callback which will be called when a 
 * packet is received.
 */
void UPacket_Init(uint8_t uart, void (*callback)(uint8_t * data, uint16_t length));

/** @brief Send a packet on the specified UART channel
 * 
 * Example
 * @code
 * uint8_t data[100] = { data to send };
 * UPacket_Send(1, &data[0], 100);
 * @endcode
 * 
 * @param uart UART index to use
 * @param data Pointer to uint8_t array of data to send
 * @param length Length in bytes of data
 */
void UPacket_Send(uint8_t uart, uint8_t * data, uint16_t length);

/** Disable a UPacket channel (unregisters the UART receiver)
 * 
 * @param uart UART index to use
 */
void UPacket_Disable(uint8_t uart);

/** Re-Enable a UPacket channel (re-registers the UART receiver)
 * 
 * Do not use this method to initialize the module, UPacket_Init() must be used.
 * 
 * This method is intended to be used after using UPacket_Disable()
 * 
 * @param uart UART index to use
 */
void UPacket_Enable(uint8_t uart);

/** Register a callback to receive data that is received outside of a UPacket
 * 
 * Useful if other data besides UPackets will be transmitted on the same UART channel.
 * For example log messages being sent on the same channel as UPacket messages
 * 
 * @param uart UART index to use
 * @param dump function pointer to a function that takes a uint8_t input and will do something with it
 */
void UPacket_RegisterDumpCallback(uint8_t uart, void(*dump)(uint8_t));

/** @} */ // uart_packet
#endif // _UART_PACKET_H_
