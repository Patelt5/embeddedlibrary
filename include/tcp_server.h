/*
 * tcp_server.h
 *
 *  Created on: Mar 1, 2016
 *      Author: Anthony
 */

#ifndef _TCP_SERVER_H_
#define _TCP_SERVER_H_

#include "hal_socket.h"

#include "system.h"

#ifdef USE_MODULE_TASK
#include "task.h"
#endif

#include "list.h"
#include "buffer.h"

#ifndef TCPSERVER_MAX_CLIENTS
/* The maximum number of clients the listener can handle at any time */
#define TCPSERVER_MAX_CLIENTS 3
#endif

#define TCPSERVER_DEFAULT_BUF_SIZE 1472

#ifndef TCPSERVER_RX_BUF_SIZE
/* The maximum length message that can be received */
#define TCPSERVER_RX_BUF_SIZE TCPSERVER_DEFAULT_BUF_SIZE
#endif

#ifndef TCPSERVER_TX_BUF_SIZE
/* The maximum amount of data that can be queued to be sent */
#define TCPSERVER_TX_BUF_SIZE TCPSERVER_DEFAULT_BUF_SIZE
#endif

#ifndef TCPSERVER_LOG_LEVEL
#define TCPSERVER_LOG_LEVEL 0
#endif

/* Sets the number of bytes used as the expected length prefix for message reconstruction */
#define TCPSERVER_HEADER_SIZE 3

enum tcpServer_updates{
    NEW_CLIENT = 0,
    LISTENER_SOCKET_FAILED,
    CLIENT_SOCKET_FAILED,
    CLIENT_DISCONNECTED
} tcpServer_updates;

typedef void (* tcpServer_statusUpdateCallback_t)(void *, enum tcpServer_updates, void *);
typedef void (* tcpServer_receiveCallback_t)(void *, struct sockaddr_in *, uint8_t *, uint16_t);

typedef struct tcpServer_client_t{
    int32_t socketID;
    struct sockaddr_in addr;
    uint8_t rxBuffer [TCPSERVER_RX_BUF_SIZE + TCPSERVER_HEADER_SIZE];
    uint16_t rxBufIndex;
    uint16_t currMsgLength;
    BUFFER_ALLOCATE(txBuffer, TCPSERVER_TX_BUF_SIZE + TCPSERVER_HEADER_SIZE);
} tcpServer_client_t;

typedef struct tcpServer_t{
    int16_t socketID;
    struct sockaddr_in addr;
    tcpServer_statusUpdateCallback_t statusUpdateCallback;
    tcpServer_receiveCallback_t receiveCallback;
    LIST_ALLOCATE(tcpServer_client_t, clientList, TCPSERVER_MAX_CLIENTS);
    uint8_t useHeader : 1; // Used to choose whether or not the server should use the header to package data
}tcpServer_t;

/**
 * @brief Initializes the TCPServer
 *
 * Initiliazes the TCPServer by setting the statusUpdatecallback and receiveCallback.
 * Typically this function will only be called once, however it could be called again
 * to update the callbacks.
 *
 * @param tcpServer - A pointer to the TCPServer.
 * @param statusUpdateCallback - A callback for receiving status updates such as connecting
 *     to a new client, or an error occuring on the listener socket.
 *     See tcpServer_statusUpdateCallback_t and tcpServer_updates_t
 * @param receiveCallback - A callback for receiving messages from client connections
 *
 * @return 0 on success
 *         -1 if an invalid argument was passed in
 */
int8_t TCPServer_Init(tcpServer_t* tcpServer, tcpServer_statusUpdateCallback_t statusUpdateCallback, tcpServer_receiveCallback_t receiveCallback, uint8_t useHeader);

/**
 * @brief Creates and configures the socket. Optionally schedules TCPServer_Tick
 * 
 * The following socket interaction is completed:
 *  - Creates a socket
 *  - Binds to the given address
 *  - Starts listening on the socket
 *  - Sets the socket to be non-blocking
 *
 * Additionally, if USE_MODULE_TASK is defined in system.h, the TCPServer_Tick function
 * is automatically scheduled to be run every 2ms.
 *
 * @param tcpServer - A pointer to the TCPServer
 * @param port - the port for the TCP socket to listen on
 * @return 0 on success
 *         -1 on socket error 
 */
int8_t TCPServer_Start(tcpServer_t* tcpServer, uint16_t port);

/**
 * @brief Handles new connections and receiving data from existing connections with clients
 *
 * @param tcpServer - A pointer to the TCPServer
 */
void TCPServer_Tick(tcpServer_t* tcpServer);

void TCPServer_Stop(tcpServer_t* tcpServer);

int8_t TCPServer_Send(tcpServer_t* tcpServer, char * msg, uint16_t length);

#endif /* _TCP_SERVER_H_ */
