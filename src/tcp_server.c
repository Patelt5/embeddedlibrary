/*
 * tcp_server.c
 *
 *  Created on: March 1, 2015
 *      Author: Anthony
 */

// Standard Includes
#include <stdint.h>

// Project Includes
#include "tcp_server.h"
#include "buffer.h"

#if defined (__ERRNO_H__) || defined (_ERRNO_H)
#define ERROR_VALUE errno
#else
#define ERROR_VALUE retVal
#endif

void AcceptNewConnections(tcpServer_t* tcpServer);
void ReceiveFromClients(tcpServer_t* tcpServer);
void SendOnClients(tcpServer_t* tcpServer);

int8_t TCPServer_Init(tcpServer_t* tcpServer,tcpServer_statusUpdateCallback_t statusUpdateCallback, tcpServer_receiveCallback_t receiveCallback, uint8_t useHeader) {
    // Check and set the status update callback
    if(!statusUpdateCallback) {
        return -1;
    }

    // Check and set the receive callback
    if(!receiveCallback) {
        return -1;
    }

    tcpServer->statusUpdateCallback = statusUpdateCallback;
    tcpServer->receiveCallback = receiveCallback;
    tcpServer->useHeader = useHeader;

    return 0;
}

int8_t TCPServer_Start(tcpServer_t* tcpServer, uint16_t port) {
    int16_t retVal;

    /* Initialize the list module. This is done here instead of in the init so that if the socket is
       stopped and then restarted, the list of clients is cleared */
    LIST_INIT(tcpServer->clientList, TCPSERVER_MAX_CLIENTS, sizeof(tcpServer_client_t));

    // Filling the TCP server socket address
    tcpServer->addr.sin_family = AF_INET;
    tcpServer->addr.sin_port = htons(port);
    tcpServer->addr.sin_addr.s_addr = 0;

    // Try and create a new socket
    retVal = socket(AF_INET, SOCK_STREAM, 0);
    if( retVal < 0 ) {
        LogError(0x00, "Failed to open TCP socket. Line [%d] Function [%s] Returned Error [%d]", __LINE__, __FUNCTION__, ERROR_VALUE);
        return -1;
    }

    // retVal is socketId if not negative
    tcpServer->socketID = retVal;

    // bind the TCP socket to the TCP server address
    retVal = bind(tcpServer->socketID, (struct sockaddr *)&tcpServer->addr, (socklen_t)sizeof(struct sockaddr_in));
    if( retVal < 0 ){
        close(tcpServer->socketID);
        LogError(0x00, "Failed to bind socket. Line [%d] Function [%s] Returned Error [%d]", __LINE__, __FUNCTION__, ERROR_VALUE);
        return -1;
    }

    // begin listening for an incoming TCP connection
    retVal = listen(tcpServer->socketID, 0);
    if( retVal < 0 )
    {
        close(tcpServer->socketID);
        LogError(0x00, "Failed to start listening on socket. Line [%d] Function [%s] Returned Error [%d]", __LINE__, __FUNCTION__, ERROR_VALUE);
        return -1;
    }

    retVal = SET_SOCKET_NONBLOCKING(tcpServer->socketID);
    if( retVal < 0 )
    {
        close(tcpServer->socketID);
        LogError(0x00, "Failed to set socket options. Line [%d] Function [%s] Returned Error [%d]", __LINE__, __FUNCTION__, ERROR_VALUE);
        return -1;
    }

#ifdef _TASK_H_
    // Schedule task to handle the connection every 2ms
    Task_Schedule(TCPServer_Tick, tcpServer, 0, 2);
#endif

    return 0;
}

void TCPServer_Tick(tcpServer_t * tcpServer) {
    AcceptNewConnections(tcpServer);
    SendOnClients(tcpServer);
    ReceiveFromClients(tcpServer);
}

void SendOnClients(tcpServer_t* tcpServer) {
    int retVal;
    tcpServer_client_t * client;
    uint16_t numBytesToWrite;

    client = List_GetFirst(&tcpServer->clientList);
    while(client != 0) {

        // Check if there is any data to be sent
        if(!GetSize(&client->txBuffer)){
            // Get the next client in the list
            client = List_GetNext(&tcpServer->clientList, client);
            continue;
        }

        /* Figure out how much data can be sent contiguously
         *
         * Since the buffer module implements a circular buffer,
         * the data that needs to be transmitted may not be in
         * a contiguous block or memory. In order to manage the
         * data, without having to shift it, we can grab the largest
         * contiguous section of memory and send that.
         *
         * There are two conditions:
         *   1) [  f     r   ] - The rear is after the front
         *     In this condition, all the data that needs to be sent is
         *     in one contiguous block.
         *   2) [ r    f     ] - The rear is before the front
         *     In this condition, there are two contiguous blocks,
         *     1 from the front to the end of the buffer, and
         *     1 from the start to the rear of the buffer.  In this case,
         *     we just send the 1st contiguous block and then the next call
         *     will handle the 2nd contiguous block
         */
        if(client->txBuffer.front < client->txBuffer.rear) {
            numBytesToWrite = GetSize(&client->txBuffer);
        } else {
            numBytesToWrite = (client->txBuffer.buffer_end - client->txBuffer.front) + 1;
        }

        retVal = send(client->socketID, client->txBuffer.front, numBytesToWrite, 0);

        /* The return value will either be a positive number for the amount of bytes sent,
         * a negative value of EAGAIN if the socket didn't send but didn't have an error,
         * or negative but not EAGAIN, on a socket error.
         */
        if(retVal >= 0) {
            // Move the front of the buffer forward to "pop" the sent data off the buffer
            client->txBuffer.front += retVal;
            if(client->txBuffer.front > client->txBuffer.buffer_end) {
                client->txBuffer.front = client->txBuffer.buffer_start;
            }
            client->txBuffer.size -= retVal;

            // Get the next client in the list
            client = List_GetNext(&tcpServer->clientList, client);
            continue;
        } else if(ERROR_VALUE != EAGAIN) {
            // There was an error, close the client, remove it from the list, and notify the application
            tcpServer_client_t* rmClient;
            rmClient = client;
            // Need to try and get the next client before removing it
            client = List_GetNext(&tcpServer->clientList, rmClient);
            close(rmClient->socketID);

            tcpServer->statusUpdateCallback(tcpServer, CLIENT_SOCKET_FAILED, rmClient);
            LogError(0x00, "TCPServer: Client socket error [%d]. Client Removed. %lu:%d", ERROR_VALUE, rmClient->addr.sin_addr, rmClient->addr.sin_port);

            List_Remove(&tcpServer->clientList, rmClient);
            continue;
        }
        // Get the next client in the list
        client = List_GetNext(&tcpServer->clientList, client);
        continue;
    }
}

void AcceptNewConnections(tcpServer_t* tcpServer) {
    int retVal;
    tcpServer_client_t newClient;
    tcpServer_client_t * client_ptr;

    socklen_t alen = sizeof(newClient.addr);
    // Check for any new connections
    retVal = accept(tcpServer->socketID, ( struct sockaddr *)&newClient.addr, &alen);

    // If we accepted a new connection
    if (retVal >= 0) {
        newClient.socketID = retVal;

        retVal = SET_SOCKET_NONBLOCKING(newClient.socketID);
        if( retVal < 0 ) {
            // Close the new socket if we fail to set it to non-blocking
            close(newClient.socketID);
        } else {
            // Indirectly add the client to the list so the buffers doesn't have to be copied
            client_ptr = List_AddIndirect(&tcpServer->clientList);

            if(client_ptr == 0) {
                LogWarning(0x00, "TCPServer: Client rejected, too many clients");
                close(newClient.socketID);
                return;
            }

            client_ptr->socketID = newClient.socketID;
            client_ptr->addr = newClient.addr;
            client_ptr->rxBufIndex = 0;
            client_ptr->currMsgLength = 0;

            // Initialize the tx buffer
            BUFFER_INIT(client_ptr->txBuffer, TCPSERVER_TX_BUF_SIZE + TCPSERVER_HEADER_SIZE);

            // Link the client into the list
            List_Link(&tcpServer->clientList);

            LogDebug(0x00, "TCPServer: Client Connected: %lu:%d", newClient.addr.sin_addr, newClient.addr.sin_port);
            tcpServer->statusUpdateCallback(tcpServer, NEW_CLIENT, &newClient);
        }
    } else if(ERROR_VALUE != EAGAIN) {

        LogError(0x00, "Socket error while attempting to accept incoming connection. Line [%d] Function [%s] Returned Error [%d]", __LINE__, __FUNCTION__, ERROR_VALUE);

        // If we got an error other than try again
        TCPServer_Stop(tcpServer);
        tcpServer->statusUpdateCallback(tcpServer, LISTENER_SOCKET_FAILED, 0);

#ifdef _TASK_H_
        Task_Remove(TCPServer_Tick, tcpServer);
#endif
    }
}

void ReceiveFromClients(tcpServer_t* tcpServer) {
    int retVal;
    tcpServer_client_t * client;

    client = List_GetFirst(&tcpServer->clientList);

    // List returns 0 when there is nothing left in the list
    while(client != 0) {
        // Flag for handling messages that are too long for the buffer
        bool msgTooLong = false;

        // Keep looping while their is data left to be read
        while(true) {

            if(tcpServer->useHeader){

            	 uint16_t numByteToRead = 0;

				// If we haven't received the length yet
				if(client->currMsgLength == 0) {
					numByteToRead = TCPSERVER_HEADER_SIZE - client->rxBufIndex;
				} else {
					numByteToRead = (client->currMsgLength + TCPSERVER_HEADER_SIZE) - client->rxBufIndex;
				}

				// Check if the current message length is too long
				if(client->currMsgLength > TCPSERVER_RX_BUF_SIZE) {
					msgTooLong = true;
				}

				// If the message we are receiving is too long, keep overwriting the data in the buffer
				if(msgTooLong) {
					retVal = recv(client->socketID, &client->rxBuffer[TCPSERVER_HEADER_SIZE], TCPSERVER_RX_BUF_SIZE, 0);
				} else {
					retVal = recv(client->socketID, &client->rxBuffer[client->rxBufIndex], numByteToRead, 0);
				}
            } else {
            	retVal = recv(client->socketID, &client->rxBuffer[0], TCPSERVER_RX_BUF_SIZE, 0);
            }

            // If the return value is positive or 0 we received that many bytes
            if(retVal > 0) {
            	if(tcpServer->useHeader){
					client->rxBufIndex += retVal;

					// If we have received the total length
					if(client->rxBufIndex == TCPSERVER_HEADER_SIZE) {
						// Length is always sent as little endian
						client->currMsgLength = client->rxBuffer[0];
						client->currMsgLength |= (((uint16_t)client->rxBuffer[1]) << 8) & 0xFF00;
						// Warn the user if the length is bigger than the buffer
						if(client->currMsgLength > TCPSERVER_RX_BUF_SIZE) {
							LogWarning(0x00, "TCPServer: Received packet larger than buffer");
						}
					}

					// If we have received the complete message
					if(client->rxBufIndex == client->currMsgLength + TCPSERVER_HEADER_SIZE) {
						if(!msgTooLong) {
							tcpServer->receiveCallback(tcpServer, &client->addr, &client->rxBuffer[TCPSERVER_HEADER_SIZE], client->currMsgLength);
						}
						client->currMsgLength = 0;
						client->rxBufIndex = 0;
					}
            	} else {
            		tcpServer->receiveCallback(tcpServer, &client->addr, &client->rxBuffer[0], retVal);
            	}
            } else if (retVal == 0) {
                tcpServer_client_t* rmClient;
                rmClient = client;
                // Need to try and get the next client before removing it
                client = List_GetNext(&tcpServer->clientList, rmClient);
                close(rmClient->socketID);

                tcpServer->statusUpdateCallback(tcpServer, CLIENT_DISCONNECTED, rmClient);

                LogMsg(0x00, "TCPServer: Client diconnected gracefully. %lu:%d", ERROR_VALUE, rmClient->addr.sin_addr, rmClient->addr.sin_port);

                List_Remove(&tcpServer->clientList, rmClient);
                break;
            } else {
                // If the socket says try again, break til next tick
                if(ERROR_VALUE == EAGAIN){
                    // Get the next client in the list
                    client = List_GetNext(&tcpServer->clientList, client);
                    break;
                }

                // There was an error, close the client, remove it from the list, and notify the application
                tcpServer_client_t* rmClient;
                rmClient = client;
                // Need to try and get the next client before removing it
                client = List_GetNext(&tcpServer->clientList, rmClient);
                close(rmClient->socketID);

                tcpServer->statusUpdateCallback(tcpServer, CLIENT_SOCKET_FAILED, rmClient);
                LogError(0x00, "TCPServer: Client socket error [%d]. Client Removed. %lu:%d", ERROR_VALUE, rmClient->addr.sin_addr, rmClient->addr.sin_port);

                List_Remove(&tcpServer->clientList, rmClient);
                break;
            }
        }
    }
}

int8_t TCPServer_Send(tcpServer_t* tcpServer, char * msg, uint16_t length) {
    tcpServer_client_t * client;
    int retVal = 0;
    char header[TCPSERVER_HEADER_SIZE];
    uint8_t i;

    // Push the data on each of the clients buffers, if any fail, return -1 but still try to push on every one
    client = List_GetFirst(&tcpServer->clientList);
    while(client != 0) {

    	if(tcpServer->useHeader) {
            // Little-endian length prefix
            header[0] = (length & 0xFF);
            header[1] = (length >> 8) & 0xFF;
            // Options-byte
            header[2] = 0x00;

    		// Try to push the length prefix onto the buffer
			if(PushData(&client->txBuffer, &header[0], TCPSERVER_HEADER_SIZE) == BUFFER_PUSH_FAILED) {
				// If pushing the length failed, the client can't send the message
				retVal--;
				LogWarning(0x00, "TCPServer: Failed to push message to client buffer, not enough room");

				// Get the next client in the list
				client = List_GetNext(&tcpServer->clientList, client);
				continue;
			}
    	}

    	if(PushData(&client->txBuffer, msg, length) == BUFFER_PUSH_FAILED) {

    		if(tcpServer->useHeader) {
				// If pushing the data length prefix succeeded but the data failed, we need to pop the length back off
				for(i = 0; i < TCPSERVER_HEADER_SIZE; i++){
					// If after decrementing the rear, we move past the front of the buffer. Set the rear to the end
					if(--client->txBuffer.rear < client->txBuffer.buffer_start) {
						client->txBuffer.rear = client->txBuffer.buffer_end;
					}
				}
				client->txBuffer.size -= TCPSERVER_HEADER_SIZE;
    		}

            // Decrement the retVal to be returned at end of function representing number of clients failed
            retVal--;
            LogWarning(0x00, "TCPServer: Failed to push message to client buffer, not enough room");
        }

        // Get the next client in the list
        client = List_GetNext(&tcpServer->clientList, client);
    }

    return retVal;
}

void TCPServer_Stop(tcpServer_t* tcpServer) {
    int retVal;
    tint_t closeTimeout;
    // Start by closing all of the client sockets
    tcpServer_client_t* client = List_GetFirst(&tcpServer->clientList);
    while(client != 0) {
        retVal = close(client->socketID);
        if(retVal < 0) {
            LogWarning(0x00, "Socket failed to close. Line [%d] Function [%s] Returned Error [%d]", __LINE__, __FUNCTION__, ERROR_VALUE);
        }
        client = List_GetNext(&tcpServer->clientList, client);
    }

    // Close the listener socket now
    closeTimeout = TimeNow();

    while(TimeSince(closeTimeout) < 2000) {
        retVal = close(tcpServer->socketID);
        if(retVal == 0) {
#ifdef _TASK_H_
        	Task_Remove(TCPServer_Tick, tcpServer);
#endif
        	return;
        }
    }

    LogWarning(0x00, "Socket failed to close. Line [%d] Function [%s] Returned Error [%d]", __LINE__, __FUNCTION__, ERROR_VALUE);
}
