#include "game_controller_host.h"
#include "game_controller.h"
#include "uart.h"
#include "uart_packet.h"
#include "subsys.h"

// internal function to handle messages received over UART
static void UPacketCallback(uint8_t * data, uint16_t length);
// internal function to check for and call any callback subscribed to the
// button data
static void CallCallback(uint8_t address, uint16_t button_data);
// internal function to add a callback to the callback list
static void AddCallbackToList(uint8_t address,
        void(*callback)(controller_buttons_t,void*),
        controller_buttons_t mask, void * handle, uint8_t only_pressed);
// helper for sending address message
static void SendSetAddr(void);

// button data for each players button
static uint16_t last_button_data[4] = {0,0,0,0};
// handle for the subsystem module (used for logging capability)
static uint8_t sys_id;

#ifndef GAME_CONTROLLER_MAX_CALLBACKS
#define GAME_CONTROLLER_MAX_CALLBACKS 16
#endif

struct {
    void(*callback)(controller_buttons_t,void*);
    controller_buttons_t mask;
    void * handle;
    uint8_t address;
    uint8_t only_pressed;
} callback_list[GAME_CONTROLLER_MAX_CALLBACKS];

void GameControllerHost_Init(void) {
    uint8_t i;
    UPacket_Init(GAME_CONTROLLER_UART, UPacketCallback);
    for(i = 0; i < GAME_CONTROLLER_MAX_CALLBACKS; i++) {
        callback_list[i].callback = 0;
    }
    #ifdef USE_MODULE_SUBSYS
    sys_id = Subsystem_Init("GCtlr", (version_t)0x01010001U, 0);
    MuteSys(sys_id);
    #endif
}

void GameControllerHost_RegisterPressCallback(uint8_t address, 
        void(*callback)(controller_buttons_t,void*),
        controller_buttons_t mask, void * handle) {
    AddCallbackToList(address,callback,mask,handle,1);
}

void GameControllerHost_RegisterCallback(uint8_t address, 
        void(*callback)(controller_buttons_t,void*),
        controller_buttons_t mask, void * handle) {
    AddCallbackToList(address,callback,mask,handle,0);
}

void GameControllerHost_SetPeriod(uint16_t period) {
    uint8_t msg[3];
    union16_t u16;
    msg[0] = CMSG_SETPERIOD;
    u16.word = period;
    msg[1] = u16.b[0];
    msg[2] = u16.b[1];
    UPacket_Send(GAME_CONTROLLER_UART, msg, 3);
}

controller_buttons_t GameControllerHost_GetButtonState(uint8_t address) {
    controller_buttons_t buttons;
    if(address <= 3) {
        buttons.all_buttons = last_button_data[address];
    }else {
        buttons.all_buttons = 0;
    }
    return buttons;
}

void GameControllerHost_RemoveCallback(void(*callback)(controller_buttons_t,void*), 
        void * handle) {
    uint8_t i, j;
    // loop through the list and look for matches to remove
    for(i = 0; i < GAME_CONTROLLER_MAX_CALLBACKS; i++) {
        // stop looking if we reach the end of the list
        if(callback_list[i].callback == 0) break;
        if(callback_list[i].callback == callback && callback_list[i].handle == handle) {
            // if this is the last callback in the list then just remove it
            if(i == GAME_CONTROLLER_MAX_CALLBACKS - 1) {
                callback_list[i].callback = 0;
            }else if(callback_list[i+1].callback == 0) {
                callback_list[i].callback = 0;
            }else {
                // otherwise copy the last callback in the list to this location
                // and remove the last callback.
                for(j = GAME_CONTROLLER_MAX_CALLBACKS-1; j > i; j--) {
                    // since we started at the end if the callback exists then it
                    // is the last one
                    if(callback_list[j].callback != 0) {
                        callback_list[i] = callback_list[j];
                        callback_list[j].callback = 0;
                        // exit the j loop
                        break;
                    }
                }
            }
        }
    }
}

void UPacketCallback(uint8_t * data, uint16_t length) {
    // check for get address message
	data++; // The first byte is blank! We're using 10 bytes in the message now.

	// LogMsg(sys_id, "Received UPacket, length %u, Data %u", length, *data);

    if(((*data) & CMSG_MASK) == CMSG_GETADDR) {
    	LogMsg(sys_id, "Got address request!");
        // respond with address message and address 0
    	// uint8_t msg[2] = {CMSG_SETADDR, 0x00};
    	SendSetAddr();
    	LogMsg(sys_id, "Sent address!");
    // check for button data message
    }else if(((*data) & CMSG_MASK) == CMSG_BUTTON) {
    	LogMsg(sys_id, "Received button packet!");
        uint8_t address = 0;
        union16_t u16;
        uint16_t buttons_changed;

        //address = (*data++) >> 5;
        data++;
        length--;
        while((address <= 3) && length) {
        	u16.b[0] = *data++;
            u16.b[1] = *data++;
            // check for a change in the data
            buttons_changed = u16.word ^ last_button_data[address];
            // update the last button data
            last_button_data[address] = u16.word;
            // if anything changed then call any matching callback
            if(buttons_changed != 0) {
                CallCallback(address,buttons_changed);
            }
            // increment the address and decrement length to process button
            // data from the next address if it exists
            address++;
            length -= 2;
        }
    } else {
    	// Received unknown UPacket
    	LogMsg(sys_id, "Received unknown UPacket, length %d, data %d", length, *data);
    }
}

static void Receiver(char c) {
	controller_buttons_t btn;
	btn.all_buttons = 0;
	switch(c) {
		case 'w':
		case 'W': // UP
			btn.button.up = 1;
			break;
		case 'a':
		case 'A': // LEFT
			btn.button.left = 1;
			break;
		case 's':
		case 'S': // DOWN
			btn.button.down = 1;
			break;
		case 'd':
		case 'D': // RIGHT
			btn.button.right = 1;
			break;
		case ' ': // A
			btn.button.A = 1;
			break;
		case 'b':
		case 'B': // B
			btn.button.B = 1;
			break;
		case '\r': // START
			btn.button.start = 1;
			break;
		case '=': // SELECT
			btn.button.select = 1;
			break;
		default:
			break;
	}
	last_button_data[0] = btn.all_buttons;
	CallCallback(0, btn.all_buttons);
	last_button_data[0] = 0;
	CallCallback(0, btn.all_buttons);
}

void GameControllerHost_EnableTerminalController(uint8_t uart_channel) {
	UART_RegisterReceiver(uart_channel, Receiver);
}

void GameControllerHost_DisableTerminalController(uint8_t uart_channel) {
	UART_UnregisterReceiver(uart_channel, Receiver);
}

static void CallCallback(uint8_t address, uint16_t button_data) {
    uint8_t i;
    controller_buttons_t buttons;
    for(i = 0; i < GAME_CONTROLLER_MAX_CALLBACKS; i++) {
        // if there is no callback then break
        if(callback_list[i].callback == 0) break;
        // check for address match first
        if(callback_list[i].address == 0x07 || callback_list[i].address == address) {
            // check for only pressed next
            if(callback_list[i].only_pressed) {
                // limit button_data to only pressed buttons
                // note last_button_data is current at this point so ANDing it
                // with the buttons that changed will tell us what was pressed
                buttons.all_buttons = button_data & last_button_data[address];
            }else {
                buttons.all_buttons = button_data;
            }
            // check for mask with buttons
            if(callback_list[i].mask.all_buttons & buttons.all_buttons) {
                callback_list[i].callback(buttons, callback_list[i].handle);
            }
        }
    }
}

static void AddCallbackToList(uint8_t address,
        void(*callback)(controller_buttons_t,void*),
        controller_buttons_t mask, void * handle, uint8_t only_pressed) {
    uint8_t i;
    // loop through callback list and find first empty spot
    for(i = 0; i < GAME_CONTROLLER_MAX_CALLBACKS; i++) {
        // add callback data to empty slot and return
        if(callback_list[i].callback == 0) {
            callback_list[i].address = address;
            callback_list[i].callback = callback;
            callback_list[i].handle = handle;
            callback_list[i].mask = mask;
            callback_list[i].only_pressed = only_pressed;
            return;
        }
    }
    // if no empty slot then log a warning
    LogMsg(sys_id, "Warning, button callback not added, no room in callback list");
}

void SendSetAddr(void) {
	uint8_t msg[10] = {0x00, 0xC0 | CMSG_SETADDR, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	// is the byte where address is stored, getting swapped?
	// 					1		2					3	  4		5	 6 		7	  8     9     10
	// NOTE: for multi-byte datatypes, the first byte we send is going to be the least significant byte
	// due to endianness / uint16
	UPacket_Send(GAME_CONTROLLER_UART, msg, 10);
}
