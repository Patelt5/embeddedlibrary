#include "bomberman.h"
#include "system.h"
#define x_offset 4
#define y_offset 4

Player Player1_default= {255, "p01", 1+x_offset*0,   1+y_offset*0,   149, graphic_clear, bombs+0*MAX_BOMBS_PER_PLAYER, ForegroundCyan, 2, 2, 0};
Player Player2_default= {255, "p02", 1+x_offset*1,   1+y_offset*0,   149, graphic_clear, bombs+1*MAX_BOMBS_PER_PLAYER, ForegroundGreen, 2, 2, 0};
Player Player3_default= {255, "p03", 1+x_offset*0,   1+y_offset*1,   149, graphic_clear, bombs+2*MAX_BOMBS_PER_PLAYER, ForegroundMagenta, 2, 2, 0};
Player Player4_default= {255, "p04", 1+x_offset*1,   1+y_offset*1,   149, graphic_clear, bombs+3*MAX_BOMBS_PER_PLAYER, ForegroundRed, 2, 2, 0};
Player Player5_default= {255, "p05", 1+x_offset*2,   1+y_offset*0,   149, graphic_clear, bombs+4*MAX_BOMBS_PER_PLAYER, ForegroundWhite, 2, 2, 0};
Player Player6_default= {255, "p06", 1+x_offset*2,   1+y_offset*1,   149, graphic_clear, bombs+5*MAX_BOMBS_PER_PLAYER, ForegroundYellow, 2, 2, 0};
Player Player7_default= {255, "p07", 1+x_offset*2,   1+y_offset*2,   64, graphic_clear, bombs+6*MAX_BOMBS_PER_PLAYER, ForegroundCyan, 2, 2, 0};
Player Player8_default= {255, "p08", 1+x_offset*1,   1+y_offset*2,   64, graphic_clear, bombs+7*MAX_BOMBS_PER_PLAYER, ForegroundGreen, 2, 2, 0};
Player Player9_default= {255, "p09", 1+x_offset*0,   1+y_offset*2,   64, graphic_clear, bombs+8*MAX_BOMBS_PER_PLAYER, ForegroundMagenta, 2, 2, 0};
Player Player10_default={255, "p10", 1+x_offset*3,   1+y_offset*0,   64, graphic_clear, bombs+9*MAX_BOMBS_PER_PLAYER, ForegroundRed, 2, 2, 0};
Player Player11_default={255, "p11", 1+x_offset*3,   1+y_offset*1,   64, graphic_clear, bombs+10*MAX_BOMBS_PER_PLAYER, ForegroundWhite, 2, 2, 0};
Player Player12_default={255, "p12", 1+x_offset*3,   1+y_offset*2,   64, graphic_clear, bombs+11*MAX_BOMBS_PER_PLAYER, ForegroundYellow, 2, 2, 0};
Player Player13_default={255, "p13", 1+x_offset*3,   1+y_offset*3,   132, graphic_clear, bombs+12*MAX_BOMBS_PER_PLAYER, ForegroundCyan, 2, 2, 0};
Player Player14_default={255, "p14", 1+x_offset*2,   1+y_offset*3,   132, graphic_clear, bombs+13*MAX_BOMBS_PER_PLAYER, ForegroundGreen, 2, 2, 0};
Player Player15_default={255, "p15", 1+x_offset*1,   1+y_offset*3,   132, graphic_clear, bombs+14*MAX_BOMBS_PER_PLAYER, ForegroundMagenta, 2, 2, 0};
Player Player16_default={255, "p16", 1+x_offset*0,   1+y_offset*3,   132, graphic_clear, bombs+15*MAX_BOMBS_PER_PLAYER, ForegroundRed, 2, 2, 0};
Player Player17_default={255, "p17", 1+x_offset*4,   1+y_offset*0,   132, graphic_clear, bombs+16*MAX_BOMBS_PER_PLAYER, ForegroundWhite, 2, 2, 0};
Player Player18_default={255, "p18", 1+x_offset*4,   1+y_offset*1,   132, graphic_clear, bombs+17*MAX_BOMBS_PER_PLAYER, ForegroundYellow, 2, 2, 0};
Player Player19_default={255, "p19", 1+x_offset*4,   1+y_offset*2,   134, graphic_clear, bombs+18*MAX_BOMBS_PER_PLAYER, ForegroundCyan, 2, 2, 0};
Player Player20_default={255, "p20", 1+x_offset*4,   1+y_offset*3,   134, graphic_clear, bombs+19*MAX_BOMBS_PER_PLAYER, ForegroundGreen, 2, 2, 0};
Player Player21_default={255, "p21", 1+x_offset*4,   1+y_offset*4,   134, graphic_clear, bombs+20*MAX_BOMBS_PER_PLAYER, ForegroundMagenta, 2, 2, 0};
Player Player22_default={255, "p22", 1+x_offset*3,   1+y_offset*4,   134, graphic_clear, bombs+21*MAX_BOMBS_PER_PLAYER, ForegroundRed, 2, 2, 0};
Player Player23_default={255, "p23", 1+x_offset*2,   1+y_offset*4,   134, graphic_clear, bombs+22*MAX_BOMBS_PER_PLAYER, ForegroundWhite, 2, 2, 0};
Player Player24_default={255, "p24", 1+x_offset*1,   1+y_offset*4,   134, graphic_clear, bombs+23*MAX_BOMBS_PER_PLAYER, ForegroundYellow, 2, 2, 0};
Player Player25_default={255, "p25", 1+x_offset*0,   1+y_offset*4,   158, graphic_clear, bombs+24*MAX_BOMBS_PER_PLAYER, ForegroundCyan, 2, 2, 0};


char* titleTile[] = {
"                                `  :                                                                ",
"                                /:+o `                                                              ",
"                              /.+y:ss+                                                              ",
"                 /o+ooo++`    oyy+s+s    ::  .:  //+/`  +++++o+++//`  `/  /    ++: /// `++.         ",
"                ./      `o    .hsd-s:   `sN. oN.`:  `+ .-    yo    +  /N:/m/  /` N:: d::.+N`        ",
"                +        :h   oMMhmoy.  /.M-/.M/:`:y``h/ `---N.`Ns :h--my:ds .:  Nh  sy/ yM`        ",
"               --  +md.  :M:smMMMdM.   :..M/-`M// yM-`Mo oMMMm :Md +M+ dy yh / : Nh  /d/ mm         ",
"               +  `NMm:  dMMMMMMMMM-  `: `Mo  N+: ms oM- dMys+ sN:`mm  y` sh-./s m+ `.M-.Ms         ",
"              :.  oMd+  oMMm::NMMMMm  /  `m`  Nh`   :NN  `` m. .``hM:  `  om:`ms d.-+ m /M/         ",
"              /  `Nd:  oMMM.  mMMMMM::`   -   Ny    /Ny    -m   .mMy .  :`+m :d+ y oy + sM`         ",
"             :`      `hMMMM.`sMMMMMMh-     `  m/ s+  s: +yhmo - .MN`-+  d`/:     : dd   mm          ",
"             /       `/mMMMMMMMMMMMMh .-   s  d`-MM  h` NMmm: h `M/ d+ :M   +os+  `MN  `My          ",
"            /`         yMMMMMMMMMMMMy h.  .m  s sM+ .y -ds/h .N  h +M/ mN  :MMMy `/My++yM/          ",
"           `/  `++:    NMMMMMMMMMMMM+:M`  yd  ` /. `d+    -y oM  ``NMosMNosNM/ /MMMM-sMMM.          ",
"           /   yMMM/   MMMMMMMMMMMMM-mN  .Md      .dM.  `.ss+mMhhmNMyyMM:dMMh   +s+/  -.`           ",
"          `:  -Mm-+-  `MMMMMMMMMMMMhsMd  yMh    -sMMMhdmMMMMMMhoNmdy` :-  `                         ",
"          +   hM++:   hMMMMMMMMMMMM+MNo`:MNoyhmMMMMs.dNmhso/:-`                                     ",
"         -:  :Mm+`  `hMMMMMMMMMMMMdmMosMMMo.mMmhs+`                                                 ",
"         +   ``    -mMN:NMMMMMMMMMMMm  oo/`                                                         ",
"        --       .yMMd. .mMMMMMd+o/-`                                                               ",
"        o     `/hMMMo     /syo-                                                                     ",
"       :- .:ohNMMNs.                                                                                ",
"       hmMMMMMNy/`                                                                                  ",
"       -Nmyo/-                                                                                      ",
0
};

/*
 * CP866 Codes
 * i = 219
 * b = 176
 * space = ' ;
 * fire = 36
 * bomb = 228
 * player = 253
 */

volatile uint8_t map_2_2[MAP_DIM_2x2][MAP_DIM_2x2]=
{
    {219,219,219,219,219,219,219,219,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,219},
    {219,219,219,219,219,219,219,219,219,219}
};


volatile uint8_t map_3_3[MAP_DIM_3x3][MAP_DIM_3x3] =
{
    {219,219,219,219,219,219,219,219,219,219,219,219,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,219,219,219,219,219,219,219,219,219,219,219,219,219}
};

volatile uint8_t map_4_4[MAP_DIM_4x4][MAP_DIM_4x4] =
{
    {219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219}
};

volatile uint8_t map_5_5[MAP_DIM_5x5][MAP_DIM_5x5] =
{
    {219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,' ',' ',176,176,219},
    {219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,' ',219,176,219,219},
    {219,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,176,219},
    {219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,176,219,219},
    {219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219,219}
};

#ifdef BOMBERMAN_SERVER
GameState gameState = {0, 2, 0, 0, GAMESTATE_WAITING_FOR_PLAYERS, SERVER_BOMBERMAN, 0, 0};
#else
#ifdef BOMBERMAN_CLIENT
GameState gameState = {0, 2, 0, 0, GAMESTATE_WAITING_FOR_PLAYERS, CLIENT_BOMBERMAN, 0};
#else
#error Declare either BOMBERMAN_SERVER or BOMBERMAN_CLIENT in system.h
#endif
#endif
Bomb Bomb_default = {0,0,graphic_bomb,2,5000, false};
Player player_default={1,1,149, graphic_clear, null, ForegroundGreen, 2, 1};


void bomberManInit()
{
    gameState.gameMode = GAMESTATE_WAITING_FOR_PLAYERS;
    gameState.numberPlayers = 0;
    gameState.activePlayersRemaining = 0;

    Game_SetColor(ForegroundWhite);
    Game_SetColor(BackgroundBlack);

    setupPlayers();

    Game_HideCursor();

    if(gameState.role == CLIENT_BOMBERMAN)
    {
        GAMEID = Game_Register("bmclient","Bomberman multiplayer game client.", prePlay, help);
        nrf24_RegisterMsgHandler(BOMBERMAN_MSG, clientMessageHandler);
    }

    else if(gameState.role == SERVER_BOMBERMAN)
    {
        GAMEID = Game_Register("bmserver", "Bomberman multiplayer game server.", prePlay, help);
        nrf24_RegisterMsgHandler(BOMBERMAN_MSG, serverMessageHandler);
    }

    else
    {
        Game_Printf("\nImproper role established, must be either CLIENT_BOMBERMAN or SERVER_BOMBERMAN.\n");
        return;
    }
}

void prePlay(void)
{
    if(gameState.role == CLIENT_BOMBERMAN)
    {
    Game_ClearScreen();
    Game_Printf("\rBomberman client will start in 2 seconds.\n\n");
    Task_Schedule(clientStartUp, 0, 2000, 0);
    }

    else if(gameState.role == SERVER_BOMBERMAN)
    {
    Game_ClearScreen();
    Game_Printf("\rBomberman server will start in 2 second.\n");
    Game_Printf("\rControls:\n");
    Game_Printf("\r B - request players\n");
    Game_Printf("\r N - start game with registered players\n");
    Game_Printf("\r R - refresh registered player list\n");
    Game_Printf("\rGame Play Instructions:\n");
    Game_Printf("\rYou are the server, sit back and watch...\n");
    Task_Schedule(serverStartUp, 0, 2000, 0);
    }
}

void serverStartUp()
{
    Game_Printf("\rServer starting up...\n");
    Game_RegisterPlayer1Receiver(handleServerInput);
    soundMessagePlay(BOMBERMAN_SOUND_MAIN_THEME);
}

void clientStartUp()
{
    Game_SetColor(ForegroundYellow);
    Game_SetColor(BackgroundBlue);
    //Move to top of screen
    Game_CharXY('t',0,0);
    Game_DrawTile(titleTile,0,0);
    Game_SetColor(ForegroundWhite);
    Game_SetColor(BackgroundBlack);
    Game_Printf("\n\n\rWaiting for server game request.\n\n");
    Game_Printf("\rGame Play Instructions:\n");
    Game_Printf("\rMove using W,A,S,D.\n\rPlace bombs with space bar.\n\rBe the last to survive to win!\n");
    Game_RegisterPlayer1Receiver(handleClientInput);
}

void calculateMapSize()
{
    if( gameState.numberPlayers <= 4)
    {
        Game_Printf("\r2x2 grid loading...\n");
        gameState.map = 2;
        gameState.mapSizeX = MAP_DIM_2x2;
        gameState.mapSizeY = MAP_DIM_2x2;
    }

    else if(gameState.numberPlayers <= 9)
    {
        Game_Printf("\r3x3 grid loading...\n");
        gameState.map = 3;
        gameState.mapSizeX = MAP_DIM_3x3;
        gameState.mapSizeY = MAP_DIM_3x3;
    }

    else if(gameState.numberPlayers <= 16)
    {
        Game_Printf("\r4x4 grid loading...\n");
        gameState.map = 4;
        gameState.mapSizeX = MAP_DIM_4x4;
        gameState.mapSizeY = MAP_DIM_4x4;
    }
    else if(gameState.numberPlayers <= 25)
    {
        Game_Printf("\r5x5 grid loading...\n");
        gameState.map = 5;
        gameState.mapSizeX = MAP_DIM_5x5;
        gameState.mapSizeY = MAP_DIM_5x5;
    }
    else
    {
        //ERROR, TOO MANY PLAYERS!
        Game_Printf("\rError, too many players!\n");
        gameState.map = null;
        gameState.mapSizeX = 0;
        gameState.mapSizeY = 0;
    }
}

void setupPlayers(void)
{
    /* There is a border around the entire arena of invincible blocks
     * all players spawn in the upper left of their own grid.
     * The map size is based on the number of players
     * 4 panels, 9 panels, 16 panels, 25 panels
     * Players should be drawn diagonally
    */
    uint8_t p = 0;
    Bomb* bomb_array = bombs;
    players[0] = Player1_default;
    players[1] = Player2_default;
    players[2] = Player3_default;
    players[3] = Player4_default;
    players[4] = Player5_default;
    players[5] = Player6_default;
    players[6] = Player7_default;
    players[7] = Player8_default;
    players[8] = Player9_default;
    players[9] = Player10_default;
    players[10] = Player11_default;
    players[11] = Player12_default;
    players[12] = Player13_default;
    players[13] = Player14_default;
    players[14] = Player15_default;
    players[15] = Player16_default;
    players[16] = Player17_default;
    players[17] = Player18_default;
    players[18] = Player19_default;
    players[19] = Player20_default;
    players[20] = Player21_default;
    players[21] = Player22_default;
    players[22] = Player23_default;
    players[23] = Player24_default;
    players[24] = Player25_default;

    for( ; p < MAX_PLAYERS;p++)
    {
        //Pointer to iterate over every bomb and powerup for initialization
        Bomb* bombsIterator = bomb_array;
        Powerup* powerupIterator = powerups;
        //Iterate over all of this players bombs and set them up
        uint8_t b = 0;
        for( ; b < MAX_BOMBS_PER_PLAYER ; b++)
        {
            bombsIterator[b].x = 0;
            bombsIterator[b].y = 0;
            bombsIterator[b].symbol = graphic_bomb;
            bombsIterator[b].duration = 3000;
            bombsIterator[b].blastSize = 3;
            bombsIterator[b].active = false;
        }

        uint8_t p = 0;
        for(; p < MAX_POWERUPS_PER_PLAYER; p++)
        {
            powerupIterator[p].x = 0;
            powerupIterator[p].y = 0;
            powerupIterator[p].symbol = graphic_powerup_bombCount;
            powerupIterator[p].type = increaseBombCount;
            powerupIterator[p].active = false;
        }

        players[p].bombArray = bombsIterator;
        bombsIterator += MAX_BOMBS_PER_PLAYER;
        powerupIterator += MAX_POWERUPS_PER_PLAYER;
    }
}



void help(void)
{
    Game_Printf("\r\nChoose between single player(1) and multiplayer(2) mode.\nIn singleplayer, move your bomberman to the goal (w,s,a,d) and destroy bricks with bombs (b)!"
            "\nIn multiplayer, bomb each other till one wins!\n\r"
            "Powerups will either increase your bomb range or the number of bombs you can place at a time\n\r");
}

uint8_t isCollidingPowerup(Player* player)
{
    uint8_t p = 0;
    for(; p < MAX_POWERUPS; p++)
    {
        if(powerups[p].active)
        {
            if(player->x == powerups[p].x && player->y == powerups[p].y)
            {
                    powerups[p].active = false;
                    powerupPlayer(player, powerups[p].type);
            }
        }
    }
    drawPowerups();
}

void powerupPlayer(Player* player, PowerupType type)
{
    switch(type)
    {
        case blastRadius:
            if(player->radius < MAX_RADIUS)
            {
                player->radius++;
            }
            break;

        case increaseBombCount:
            if(player->bombCount < MAX_BOMBS_PER_PLAYER)
                player->bombCount++;
            break;
    }
}

uint8_t isValidWorldCoordinate(uint8_t x, uint8_t y)
{
	if(x >= gameState.mapSizeX || y >= gameState.mapSizeY)
            return false;

	return true;
}

uint8_t isCollidingEnvironment(uint8_t x, uint8_t y)
{

    uint8_t locationSymbol;

    switch(gameState.map)
    {
        case map_2x2:
            locationSymbol = map_2_2[x][y];
            break;
        case map_3x3:
            locationSymbol = map_3_3[x][y];
            break;
        case map_4x4:
            locationSymbol = map_4_4[x][y];
            break;
        case map_5x5:
            locationSymbol = map_5_5[x][y];
            break;
    }

    if (locationSymbol == graphic_indestructible || locationSymbol == graphic_brick )
    {
        return true;
    }

    if(isCollidingBomb(x,y))
    {
        return true;
    }

    return false;
}

uint8_t isCollidingBomb(uint8_t x, uint8_t y)
{
    uint8_t p = 0;
    uint8_t b = 0;
    //Iterate over players
    for( ; p < gameState.numberPlayers; p++)
    {
        //Iterate over all player's bombs
        for( ; b < players[p].bombCount ; b++)
        {
            if(bombs[ p * MAX_BOMBS_PER_PLAYER + b].active)
            {
                if( x == bombs[ p * MAX_BOMBS_PER_PLAYER + b].x && y == bombs[ p * MAX_BOMBS_PER_PLAYER + b].y)
                {
                    return true;
                }
            }
        }
        b = 0;
    }
    return false;
}

void debug_register4Players(){
    serverHandleRegisterPlayer(MASTER);
    serverHandleRegisterPlayer(LECAKES);
    serverHandleRegisterPlayer(ALEYAN);
    serverHandleRegisterPlayer(AMRITKAR);
}

void debug_register9Players(){
    serverHandleRegisterPlayer(MASTER);
    serverHandleRegisterPlayer(LECAKES);
    serverHandleRegisterPlayer(ALEYAN);
    serverHandleRegisterPlayer(AMRITKAR);
    serverHandleRegisterPlayer(CANDELARIA);
    serverHandleRegisterPlayer(CARLUCCIO);
    serverHandleRegisterPlayer(DONOW);
    serverHandleRegisterPlayer(LECAKES_ALL);
    serverHandleRegisterPlayer(BLAZEJEWSKI);
}

void debug_register16Players(){
    serverHandleRegisterPlayer(MASTER);
    serverHandleRegisterPlayer(LECAKES);
    serverHandleRegisterPlayer(ALEYAN);
    serverHandleRegisterPlayer(AMRITKAR);
    serverHandleRegisterPlayer(CANDELARIA);
    serverHandleRegisterPlayer(CARLUCCIO);
    serverHandleRegisterPlayer(DONOW);
    serverHandleRegisterPlayer(LECAKES_ALL);
    serverHandleRegisterPlayer(BLAZEJEWSKI);
    serverHandleRegisterPlayer(HAAS);
    serverHandleRegisterPlayer(HARRIS);
    serverHandleRegisterPlayer(HENSHAW);
    serverHandleRegisterPlayer(JACOBSEN);
    serverHandleRegisterPlayer(KLODNICKI);
    serverHandleRegisterPlayer(BLAZEJEWSKI_ALL);
    serverHandleRegisterPlayer(MORRIS);
}

void debug_register25Players(){
    serverHandleRegisterPlayer(MASTER);
    serverHandleRegisterPlayer(LECAKES);
    serverHandleRegisterPlayer(ALEYAN);
    serverHandleRegisterPlayer(AMRITKAR);
    serverHandleRegisterPlayer(CANDELARIA);
    serverHandleRegisterPlayer(CARLUCCIO);
    serverHandleRegisterPlayer(DONOW);
    serverHandleRegisterPlayer(LECAKES_ALL);
    serverHandleRegisterPlayer(BLAZEJEWSKI);
    serverHandleRegisterPlayer(HAAS);
    serverHandleRegisterPlayer(HARRIS);
    serverHandleRegisterPlayer(HENSHAW);
    serverHandleRegisterPlayer(JACOBSEN);
    serverHandleRegisterPlayer(KLODNICKI);
    serverHandleRegisterPlayer(BLAZEJEWSKI_ALL);
    serverHandleRegisterPlayer(MORRIS);
    serverHandleRegisterPlayer(LABARCK);
    serverHandleRegisterPlayer(LIU);
    serverHandleRegisterPlayer(RITCHIE_III);
    serverHandleRegisterPlayer(RUPP);
    serverHandleRegisterPlayer(RUSSO);
    serverHandleRegisterPlayer(MORRIS_ALL);
    serverHandleRegisterPlayer(TRAFFORD);
    serverHandleRegisterPlayer(WHALEN);
    serverHandleRegisterPlayer(WIBLE);
    serverHandleRegisterPlayer(MACCARONE);
    serverHandleRegisterPlayer(VOTTA);
    serverHandleRegisterPlayer(TRAFFORD_ALL);
    serverHandleRegisterPlayer(MERLINO);
    serverHandleRegisterPlayer(GOODMAN);
    serverHandleRegisterPlayer(ALL_ALL);
}

void handleServerInput(char input)
{
    if(gameState.gameMode == GAMESTATE_WAITING_FOR_PLAYERS)
    {
        switch(input)
        {
            case 'n':
            case 'N':
                //Start a new game
                
                if(gameState.numberPlayers >= 1)
                {
                    Game_Printf("\rServer is starting a new game with %d players.\n", gameState.numberPlayers);
                    serverHandleGameStart();
                }
                else
                {
                    Game_Printf("\r Cannot start game with 0 players.\n");
                }
                break;

            case 'b':
            case 'B':
                //Broadcast to all players a request to play
                Game_Printf("\rServer is requesting players.\n");
                serverMessageRequestPlayers();
                break;

            case 'r':
            case 'R':
                printRegisteredPlayers();
                break;

            case 'e':
            case 'E':
                e_addNewPlayer();
                break;
            //MOVE THE CURSOR
            //Up
            case 'w':
            case'W':
                //Check move
                soundMessagePlay(10);
                break;

            //Down
            case 's':
            case'S':
                soundMessagePlay(11);
                break;

            //Left
            case 'a':
            case'A':
                soundMessagePlay(12);
                break;
        }
    }

    else if(gameState.gameMode == GAMESTATE_GAME_PLAYING)
    {
        switch(input)
        {
            //MOVE THE CURSOR
            //Up
            case 'w':
            case'W':
                //Check move
                soundMessagePlay(10);
                break;

            //Down
            case 's':
            case'S':
                soundMessagePlay(11);
                break;

            //Left
            case 'a':
            case'A':
                soundMessagePlay(12);
                break;

            //Right
            case 'd':
            case'D':
                //Check move
                break;
                
            case ' ':
                e_placeBombPlayer1(input);
                break;

            case 'b':
            case 'B':
                e_placeBombPlayer2(input);
                break;

            case 'p':
            case 'P':
                soundMessagePlay(BOMBERMAN_SOUND_BATTLE_THEME);
                break;

            default:
                e_moveClient1(input);
                e_moveClient2(input);
        }
    }
}

void handleClientInput(char input)
{
    if(gameState.gameMode == GAMESTATE_WAITING_FOR_PLAYERS)
    {
        switch(input)
        {
            //For Emulation Testings without a network
            case 'r':
            case 'R':
                e_serverMessageRequestPlayers();
                Game_Printf("\rServer Message Rrequest Players Emulated.");
                break;
            case 't':
            case 'T':
                e_serverMessageSetPlayer(0);
                Game_Printf("\rServer Message Set Player Emulated.");
                break;
            case 'y':
            case 'Y':
                e_serverMessageGameStart(1);
                Game_Printf("\rServer Message Game Start Emulated.");
                break;
        }
    }
    else if(gameState.gameMode == GAMESTATE_GAME_PLAYING)
    {
        switch(input){
            //Up
            case 'w':
            case'W':
                //Check move
                if(clientValidateMove(MOVE_UP))
                    clientMessageMove(MOVE_UP);
                break;

            //Down
            case 's':
            case'S':
                //Check move
                if(clientValidateMove(MOVE_DOWN))
                    clientMessageMove(MOVE_DOWN);
                break;

            //Left
            case 'a':
            case'A':
                //Check move
                if(clientValidateMove(MOVE_LEFT))
                    clientMessageMove(MOVE_LEFT);
                break;

            //Right
            case 'd':
            case'D':
                //Check move
                if(clientValidateMove(MOVE_RIGHT))
                    clientMessageMove(MOVE_RIGHT);
                break;

            case ' ':
                if(clientValidatePlaceBomb())
                    clientMessagePlaceBomb();
                break;

                //Emulation
            case 'i':
                //e_serverMessagePlaceBomb();
                break;
            case 'o':
                //e_serverMessagExplodeBomb();
                break;
            case 'p':
                //e_serverMessageMovePlayer();
                break;
            case 'j':
                //e_serverMessageKillPlayer();
                break;
            case 'k':
                //e_serverMessageDestroyBlock();
                break;
            case 'l':
                //e_serverMessageplace
                break;
            case 'b':

                break;
            case 'n':

                break;
            case 'm':

                break;
        }
    }
}

void drawMap(void)
{
    switch(gameState.map)
    {
        case map_2x2:
            graphics_draw2x2Map();
            break;
        case map_3x3:
            graphics_draw3x3Map();
            break;
        case map_4x4:
            graphics_draw4x4Map();
            break;
        case map_5x5:
            graphics_draw5x5Map();
            break;
    }      
}

void clearPlayer(Player* player)
{
    Game_CharXY(' ', player->x, player->y);
}

void drawPlayerUnderneath(Player* player)
{
    Game_CharXY(player->underneath, player->x, player->y);
}

void refreshScreen()
{
    if(gameState.gameMode != GAMESTATE_GAME_OVER)
    {
        graphics_drawMap(gameState.map);
        graphics_drawPlayers();
        graphics_drawPowerups();
        graphics_drawBombs();
        graphics_listActivePlayers(playerListXOffset, playerListYOffset);
    }
}

void drawPowerups()
{
    Game_SetColor(ForegroundCyan);
    uint8_t p = 0;
    for( ; p < MAX_POWERUPS; p++)
    {
        if(powerups[p].active)
        {

            Game_CharXY(powerups[p].symbol, powerups[p].x, powerups[p].y);
        }
    }
}

void printRegisteredPlayers(void)
{
    Game_ClearScreen();
    Game_CharXY(' ',0,0);
    Game_Printf("\rRegistered Players\n");
    uint8_t i = 0;
    //Iterate over every valid player in the game
    for( ; i < MAX_PLAYERS ; i++ )
    {   
        Game_Printf("\rPlayer Name: %s", getName(players[i].address));
        Game_Printf(" : Player Address: %d\n", players[i].address);
    }
}

void serverMessageHandler(uint8_t* data, uint8_t length, uint8_t from)
{
    //The first byte of the messsage is the sub-message, identifying what we are receiving

    uint8_t message = *data++;
    uint8_t playerIndex;
    uint8_t direction;

    //Game_Printf("Server has received message from: %d", from);


    switch (gameState.gameMode)
    {
        case GAMESTATE_WAITING_FOR_PLAYERS:

            switch(message)
            {
                case CLIENT_REGISTER_PLAYER:
                    serverHandleRegisterPlayer(from);
                    break;
            }
            /*
                case SPEED_TEST:
                    serverSpeedTest();
                    break;
             */
        break;

        case GAMESTATE_GAME_PLAYING:
            switch(message)
            {
                case CLIENT_PLACE_BOMB:
                    playerIndex = *data++;
                    serverHandlePlaceBomb(playerIndex);
                    break;

                case CLIENT_MOVE:
                    playerIndex = *data++;
                    direction = *data++;
                    serverHandleMovePlayer(playerIndex, direction);
                    break;
            }
        break;
    }
}

void clientMessageHandler(uint8_t* data, uint8_t length, uint8_t from)
{
    //The first byte of the messsage is the sub-message, identifying what we are receiving

    //Game_Printf("Client has received message from: %d", from);

    uint8_t message = *data++;

    switch(gameState.gameMode)
    {

        case GAMESTATE_WAITING_FOR_PLAYERS:

            switch(message)
            {
                case SERVER_REQUEST_PLAYERS:
                    clientMessageRegisterPlayer();
                    break;
                case SERVER_SET_PLAYER:;
                    //retreive our player number
                    uint8_t playerIndex = *data++;
                    clientRegisterPlayer(playerIndex);
                    break;
                case SERVER_GAME_START:;
                    uint8_t numberOfPlayers = *data++;
                    clientHandleGameStart(numberOfPlayers);
                    break;
                    /*
                case SPEED_TEST:
                    clientSpeedTest();
                    break;
                     */
            }
            break;

        case GAMESTATE_GAME_PLAYING:
        case GAMESTATE_GAME_SPECTATE:;

            uint8_t x;
            uint8_t y;
            uint8_t playerIndex;
            uint8_t powerupType;
            uint8_t blastRadius;
            uint8_t direction;

            switch(message)
            {
                case SERVER_PLACE_BOMB:;
                    x = *data++;
                    y = *data++;
                    clientHandlePlaceBomb(x, y);
                    break;
                case SERVER_EXPLODE_BOMB:;
                    x = *data++;
                    y = *data++;
                    blastRadius = *data++;
                    clientHandleExplodeBomb(x, y, blastRadius);
                    break;
                case SERVER_MOVE_PLAYER:;
                    playerIndex = *data++;
                    direction = *data++;
                    clientHandleMovePlayer(playerIndex, direction);
                    break;
                case SERVER_KILL_PLAYER:;
                    playerIndex= *data++;
                    clientHandleKillPlayer(playerIndex);
                    break;
                case SERVER_DESTROY_BLOCK:;
                    x = *data++;
                    y = *data++;
                    clientHandleDestroyBlock(x,y);
                    break;
                case SERVER_PLACE_POWERUP:;
                    powerupType = *data++;
                    x = *data++;
                    y = *data++;
                    clientHandlePlacePowerup(powerupType, x, y);
                    break;
                case SERVER_POWERUP_PLAYER:;
                    powerupType = *data++;
                    clientHandlePowerupPlayer(powerupType);
                    break;
                case SERVER_GAME_OVER:;
                    playerIndex = *data++;
                    clientHandleGameOver(playerIndex);
                    break;
                case SERVER_REMOVE_POWERUP:;
                    x = *data++;
                    y = *data++;
                    clientHandleRemovePowerup(x,y);
                    break;
            }
            break;
    }
}

void serverHandleRegisterPlayer(uint8_t address){
    uint8_t p = 0;
    //Iterate through every player and find the next active one
    //If none is found, return false
    for( ; p < MAX_PLAYERS; p++)
    {
        if(!players[p].active){
            players[p].address = address;
            players[p].active = true;
            serverMessageSetPlayer(address, p);
            gameState.numberPlayers++;
            //printRegisteredPlayers();
            break;
        }
    }

 }

void serverTestForPowerup(uint8_t x, uint8_t y)
{

    //Is this even a powerup spot?
    if(serverIsPowerup())
    {
        //Do we have any free powerups?
        uint8_t p = 0;
        for( ; p < MAX_POWERUPS; p++)
        {
            if( !powerups[p].active)
            {
                //Get a random type of powerup
                uint8_t type = serverGetRandomPowerup();
                powerups[p].active = true;
                powerups[p].x = x;
                powerups[p].y = y;
                powerups[p].type = type;

                if(type == blastRadius)
                    powerups[p].symbol = graphic_powerup_blastRadius;

                else if(type == increaseBombCount)
                    powerups[p].symbol = graphic_powerup_bombCount;
                //Only draw the new powerup
                graphics_drawPowerupByIndex(p);

                break;
            }
        }
    }
}

uint8_t serverIsPowerup(void)
{
    int r = random_int(0, 4);

    if(r == 0)
    {
        return true;
    }

    return false;
}

uint8_t serverGetRandomPowerup()
{
    int r = random_int(blastRadius, increaseBombCount);
    return r;
}

void serverHandleMovePlayer(uint8_t playerIndex, uint8_t direction)
{

    if(!players[playerIndex].active)
    {
        return;  //Ignore things from dead players
    }

    uint8_t x = players[playerIndex].x;
    uint8_t y = players[playerIndex].y;

    Game_CharXY(' ', 25, 25);
    Game_Printf("%d,%d",x,y);

    switch(direction)
    {
        case MOVE_UP:
            y--;
            break;
        case MOVE_DOWN:
            y++;
            break;
        case MOVE_LEFT:
            x--;
            break;
        case MOVE_RIGHT:
            x++;
            break;
    }
    Game_CharXY(' ', 25, 26);
    Game_Printf("%d,%d",x,y);
    Game_CharXY(' ', 25, 27);
    Game_Printf("Index:%d",playerIndex);
    if(isValidWorldCoordinate(x,y))
    {
        if(!isCollidingEnvironment(x,y))
        {
            Game_CharXY(graphic_clear, players[playerIndex].x, players[playerIndex].y);
            players[playerIndex].x = x;
            players[playerIndex].y = y;
            serverMessageMovePlayer(playerIndex, direction);
            graphics_drawPlayers();
            graphics_drawBombs();

        }
    }
}

void serverExplodeBomb(void* b)
{
    soundMessagePlay(BOMBERMAN_SOUND_BOMB);
    Bomb* bomb = (Bomb*)b;
    switch(gameState.map)
    {
        case map_2x2:
            serverExplodeBomb_2x2(bomb);
            break;
        case map_3x3:
            serverExplodeBomb_3x3(bomb);
            break;
        case map_4x4:
            serverExplodeBomb_4x4(bomb);
            break;
        case map_5x5:
            serverExplodeBomb_5x5(bomb);
            break;
    }
}

uint8_t serverFindSurvivingPlayer()
{
    uint8_t i = 0;
    for( ; i < gameState.numberPlayers ; i++ )
    {
        if(players[i].active)
            return i;
    }
}

uint8_t serverCheckPlayersLeft()
{
    if(gameState.activePlayersRemaining <= 1)
    {
        return true;
    }
    return false;
}

void serverHandlePlayerDeath(uint8_t playerIndex)
{
    gameState.activePlayersRemaining--;
    players[playerIndex].active = false;
    soundMessagePlay(BOMBERMAN_SOUND_DIE);
    serverMessageKillPlayer(playerIndex);

    //Is this game over?
    if(serverCheckPlayersLeft())
    {
        serverHandleGameOver( serverFindSurvivingPlayer() );
    }
}

void serverExplodeBomb_2x2(Bomb* bomb)
{

    bomb->active = false;

    uint8_t i = 0;
    uint8_t p = 0;

    uint8_t x = 0;
    uint8_t y = 0;



    //Check North
    for(;i <= bomb->blastSize; i++){

        x = bomb->x;
        y = bomb->y - i;
        //Indestructibles stop fire
        if(map_2_2[x][y] == graphic_indestructible)
        {
            break;
        }
        
        //Bricks are burned and stop fire
        if(map_2_2[x][y] == graphic_brick)
        {
            map_2_2[x][y] = graphic_clear;
            serverTestForPowerup(x, y);
            break;
        }
        
        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;

    //Check South
    for(;i <= bomb->blastSize; i++){

        x = bomb->x;
        y = bomb->y + i;
        //Indestructibles stop fire
        if(map_2_2[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_2_2[x][y] == graphic_brick)
        {
            map_2_2[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;
    //Check East
    for(;i <= bomb->blastSize; i++){

        x = bomb->x+i;
        y = bomb->y;
        //Indestructibles stop fire
        if(map_2_2[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_2_2[x][y] == graphic_brick)
        {
            map_2_2[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;
    //Check West
    for(;i <= bomb->blastSize; i++){

        x = bomb->x-i;
        y = bomb->y;
        //Indestructibles stop fire
        if(map_2_2[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_2_2[x][y] == graphic_brick)
        {
            map_2_2[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    //Clear the fire from the screen
    Task_Schedule(refreshScreen, 0, 500, 0);
}

void serverExplodeBomb_3x3(Bomb* bomb)
{
    bomb->active = false;

    uint8_t i = 0;
    uint8_t p = 0;

    uint8_t x = 0;
    uint8_t y = 0;

    //Check North
    for(;i <= bomb->blastSize; i++){

        x = bomb->x;
        y = bomb->y - i;
        //Indestructibles stop fire
        if(map_3_3[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_3_3[x][y] == graphic_brick)
        {
            map_3_3[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;

    //Check South
    for(;i <= bomb->blastSize; i++){

        x = bomb->x;
        y = bomb->y + i;
        //Indestructibles stop fire
        if(map_3_3[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_3_3[x][y] == graphic_brick)
        {
            map_3_3[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;
    //Check East
    for(;i <= bomb->blastSize; i++){

        x = bomb->x+i;
        y = bomb->y;
        //Indestructibles stop fire
        if(map_3_3[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_3_3[x][y] == graphic_brick)
        {
            map_3_3[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;
    //Check West
    for(;i <= bomb->blastSize; i++){

        x = bomb->x-i;
        y = bomb->y;
        //Indestructibles stop fire
        if(map_3_3[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_3_3[x][y] == graphic_brick)
        {
            map_3_3[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    //Clear the fire from the screen
    Task_Schedule(refreshScreen, 0, 500, 0);
}

void serverExplodeBomb_4x4(Bomb* bomb)
{
    bomb->active = false;

    uint8_t i = 0;
    uint8_t p = 0;

    uint8_t x = 0;
    uint8_t y = 0;

    //Check North
    for(;i <= bomb->blastSize; i++){

        x = bomb->x;
        y = bomb->y - i;
        //Indestructibles stop fire
        if(map_4_4[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_4_4[x][y] == graphic_brick)
        {
            map_4_4[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;

    //Check South
    for(;i <= bomb->blastSize; i++){

        x = bomb->x;
        y = bomb->y + i;
        //Indestructibles stop fire
        if(map_4_4[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_4_4[x][y] == graphic_brick)
        {
            map_4_4[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;
    //Check East
    for(;i <= bomb->blastSize; i++){

        x = bomb->x+i;
        y = bomb->y;
        //Indestructibles stop fire
        if(map_4_4[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_4_4[x][y] == graphic_brick)
        {
            map_4_4[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;
    //Check West
    for(;i <= bomb->blastSize; i++){

        x = bomb->x-i;
        y = bomb->y;
        //Indestructibles stop fire
        if(map_4_4[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_4_4[x][y] == graphic_brick)
        {
            map_4_4[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    //Clear the fire from the screen
    Task_Schedule(refreshScreen, 0, 500, 0);
}

void serverExplodeBomb_5x5(Bomb* bomb)
{
    bomb->active = false;

    uint8_t i = 0;
    uint8_t p = 0;

    uint8_t x = 0;
    uint8_t y = 0;

    //Check North
    for(;i <= bomb->blastSize; i++){

        x = bomb->x;
        y = bomb->y - i;
        //Indestructibles stop fire
        if(map_5_5[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_5_5[x][y] == graphic_brick)
        {
            map_5_5[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;

    //Check South
    for(;i <= bomb->blastSize; i++){

        x = bomb->x;
        y = bomb->y + i;
        //Indestructibles stop fire
        if(map_5_5[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_5_5[x][y] == graphic_brick)
        {
            map_5_5[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;
    //Check East
    for(;i <= bomb->blastSize; i++){

        x = bomb->x+i;
        y = bomb->y;
        //Indestructibles stop fire
        if(map_5_5[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_5_5[x][y] == graphic_brick)
        {
            map_5_5[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    i = 0;
    x = 0;
    y = 0;
    //Check West
    for(;i <= bomb->blastSize; i++){

        x = bomb->x-i;
        y = bomb->y;
        //Indestructibles stop fire
        if(map_5_5[x][y] == graphic_indestructible)
        {
            break;
        }

        //Bricks are burned and stop fire
        if(map_5_5[x][y] == graphic_brick)
        {
            map_5_5[x][y] = graphic_clear;
            serverTestForPowerup(x,y);
            break;
        }

        //Players die from fire
        for( ; p < gameState.numberPlayers; p++)
        {
            if( players[p].active && players[p].x == x && players[p].y == y )
            {
                //Kill the player
                serverHandlePlayerDeath(p);
            }
        }
        p = 0;
        //Burn area
        Game_CharXY(graphic_fire, x, y);
    }

    //Clear the fire from the screen
    Task_Schedule(refreshScreen, 0, 500, 0);
}

void serverHandlePlaceBomb(uint8_t playerIndex)
{

    if(!players[playerIndex].active)
        return;
    
    uint8_t bombIndexStart = playerIndex * MAX_BOMBS_PER_PLAYER;

    uint8_t i = 0;

    for( ; i < players[playerIndex].bombCount; i++)
    {
        if(!bombs[bombIndexStart+i].active)
        {
            bombs[bombIndexStart+i].active = true;
            bombs[bombIndexStart+i].x = players[playerIndex].x;
            bombs[bombIndexStart+i].y = players[playerIndex].y;
            bombs[bombIndexStart+i].blastSize = players[playerIndex].radius;
            serverMessagePlaceBomb(players[playerIndex].x, players[playerIndex].y);
            graphics_drawBombs();
            //launch a task to execute to blow up the bomb in the future

            Task_Schedule(serverExplodeBomb, &(bombs[bombIndexStart+i]), BOMB_DURATION, 0);
            break;
        }
    }
}

void clientRegisterPlayer(uint8_t playerIndex)
{
    gameState.playerIndex = playerIndex;
    players[playerIndex].active = true;
}

void clientHandleGameStart(uint8_t numberOfPlayers)
{
    Game_HideCursor();
    gameState.gameMode = GAMESTATE_GAME_PLAYING;
    gameState.numberPlayers = numberOfPlayers;
    gameState.activePlayersRemaining = gameState.numberPlayers;
    //Activate all players
    uint8_t i = 0;
    for(; i < numberOfPlayers; i++)
    {
        players[i].active = true;
    }

    graphics_listActivePlayers(playerListXOffset,playerListYOffset);
    calculateMapSize();
    Game_ClearScreen();
    drawMap();
    graphics_drawPlayers();
}

void clientHandlePlaceBomb(uint8_t x, uint8_t y)
{
    graphics_drawBomb(x, y);
}

void clientHandleExplodeBomb(uint8_t x, uint8_t y, uint8_t blastRadius)
{
    switch(gameState.map)
    {
        case map_2x2:
            graphics_burn2x2(x, y, blastRadius);
            break;
        case map_3x3:
            graphics_burn3x3(x, y, blastRadius);
            break;
        case map_4x4:
            graphics_burn4x4(x, y, blastRadius);
            break;
        case map_5x5:
            graphics_burn5x5(x, y, blastRadius);
            break;
    }
}

void clientHandleMovePlayer(uint8_t playerIndex, uint8_t direction)
{

    Game_CharXY(graphic_clear, players[playerIndex].x, players[playerIndex].y);

    switch(direction)
    {
        case MOVE_UP:
            players[playerIndex].y--;
            break;
        case MOVE_DOWN:
            players[playerIndex].y++;
            break;
        case MOVE_LEFT:
            players[playerIndex].x--;
            break;
        case MOVE_RIGHT:
            players[playerIndex].x++;
            break;
    }

    graphics_drawPlayers();

}

void clientHandleKillPlayer(uint8_t playerIndex)
{
    players[playerIndex].active = false;

    if(gameState.playerIndex == playerIndex)
    {
        //draw game over
        gameState.gameMode = GAMESTATE_GAME_SPECTATE;
    }
    refreshScreen();
    graphics_drawYouHaveDied(0,30);
}

void clientHandleDestroyBlock(uint8_t x, uint8_t y)
{
    switch(gameState.map)
    {
        Game_Printf("clientHandleDestroyBlock %d %d", x, y);
        case map_2x2:
            map_2_2[x][y] = graphic_clear;
            graphics_drawClear(x,y);
            break;
        case map_3x3:
            map_3_3[x][y] = graphic_clear;
            graphics_drawClear(x,y);
            break;
        case map_4x4:
            map_4_4[x][y] = graphic_clear;
            graphics_drawClear(x,y);
            break;
        case map_5x5:
            map_5_5[x][y] = graphic_clear;
            graphics_drawClear(x,y);
            break;
    }
}

void clientHandlePlacePowerup(uint8_t powerupType, uint8_t x, uint8_t y)
{
    switch(gameState.map)
    {
        case map_2x2:
            switch(powerupType)
            {
                case blastRadius:
                    map_2_2[x][y] = graphic_powerup_blastRadius;
                    break;
                case increaseBombCount:
                    map_2_2[x][y] = graphic_powerup_bombCount;
                    break;
            }
            graphics_drawPowerup(powerupType, x,y);
            break;
            
        case map_3x3:
            switch(powerupType)
            {
                case blastRadius:
                    map_3_3[x][y] = graphic_powerup_blastRadius;
                    break;
                case increaseBombCount:
                    map_3_3[x][y] = graphic_powerup_bombCount;
                    break;
            }
            graphics_drawPowerup(powerupType, x,y);
            break;

        case map_4x4:
               switch(powerupType)
            {
                case blastRadius:
                    map_4_4[x][y] = graphic_powerup_blastRadius;
                    break;
                case increaseBombCount:
                    map_4_4[x][y] = graphic_powerup_bombCount;
                    break;
            }
            graphics_drawPowerup(powerupType, x,y);
            break;

        case map_5x5:
            switch(powerupType)
            {
                case blastRadius:
                    map_5_5[x][y] = graphic_powerup_blastRadius;
                    break;
                case increaseBombCount:
                    map_5_5[x][y] = graphic_powerup_bombCount;
                    break;
            }
            graphics_drawPowerup(powerupType, x,y);
            break;
    }
}

void clientHandleRemovePowerup(uint8_t x, uint8_t y)
{
    Game_CharXY(' ' , x, y);
}

void clientIncreaseBombCount()
{
    uint8_t temp = players[gameState.playerIndex].bombCount++;
    if(temp > MAX_BOMBS_PER_PLAYER)
        return;

    players[gameState.playerIndex].bombCount = temp;
}

void clientIncreaseBombRadius()
{
    uint8_t temp = players[gameState.playerIndex].bombCount++;
    if(temp > MAX_BOMBS_PER_PLAYER)
        return;

    players[gameState.playerIndex].bombCount = temp;
}

void clientHandlePowerupPlayer( uint8_t powerupType)
{
    switch(powerupType)
    {
        case blastRadius:
            clientIncreaseBombRadius();
            break;

        case increaseBombCount:
            clientIncreaseBombCount();
            break;
    }
}

void serverHandleGameStart()
{
    serverMessageGameStart(gameState.numberPlayers);
    gameState.activePlayersRemaining = gameState.numberPlayers;
    soundMessagePlay(BOMBERMAN_SOUND_GAME_START);

    //Activate all players
    uint8_t i = 0;
    for(; i < gameState.numberPlayers; i++)
    {
        players[i].active = true;
    }

    gameState.gameMode = GAMESTATE_GAME_PLAYING;
    calculateMapSize();
    Game_ClearScreen();
    drawMap();
    graphics_drawPlayers();
    graphics_listActivePlayers(playerListXOffset,playerListYOffset);
}

void serverIncreaseBombCount( uint8_t playerIndex)
{
    uint8_t temp = players[playerIndex].bombCount++;
    if(temp > MAX_BOMBS_PER_PLAYER)
        return;
    
    players[playerIndex].bombCount = temp;
}

void serverIncreaseBombRadius( uint8_t playerIndex)
{
    uint8_t temp = players[playerIndex].bombCount++;
    if(temp > MAX_BOMBS_PER_PLAYER)
        return;

    players[playerIndex].bombCount = temp;
}

void serverPowerupPlayer(uint8_t playerIndex, uint8_t powerupType)
{
    switch(powerupType)
    {
        case blastRadius:
            serverIncreaseBombRadius(playerIndex);
            break;

        case increaseBombCount:
            serverIncreaseBombCount(playerIndex);
            break;
    }
}

void serverHandlePlayerOnItem(uint8_t playerIndex)
{
    //Look at player's location and see if there is a powerup on it
    uint8_t x = players[gameState.playerIndex].x;
    uint8_t y = players[gameState.playerIndex].y;

    int i = 0;

    for( ; i < MAX_POWERUPS; i++)
    {
        if(powerups[i].active)
        {
            if( x == powerups[i].x && y == powerups[i].y)
            {
                serverPowerupPlayer(playerIndex, powerups[i].type);
                serverMessagePowerupPlayer(players[playerIndex].address, powerups[i].type);
                powerups[i].active = false;
                serverMessageRemovePowerup(x,y);
            }
        }
    }
}

uint8_t clientValidateMove(uint8_t direction)
{
    uint8_t x = players[gameState.playerIndex].x;
    uint8_t y = players[gameState.playerIndex].y;

    switch(direction)
    {
        case MOVE_UP:
            y -= 1;
            break;
        case MOVE_DOWN:
            y += 1;
            break;
        case MOVE_LEFT:
            x -= 1;
            break;
        case MOVE_RIGHT:
            x += 1;
            break;
        case MOVE_NONE:
            break;
        default:
            Game_Printf("\nUnknown direction passed into clientValidateMove.\n");
    }

    //Clients can only place bombs at their location
    if( isValidWorldCoordinate(x, y) && !isCollidingEnvironment(x,y))
    {
        return true;
    }
    return false;

}

uint8_t clientValidatePlaceBomb()
{
    //Clients can only place bombs at their location
    uint8_t x = players[gameState.playerIndex].x;
    uint8_t y = players[gameState.playerIndex].y;

    if(clientValidateMove(MOVE_NONE))
        return true;

    return false;
}

void serverMessageSetPlayer(uint8_t address, uint8_t playerIndex)
{
    uint8_t data[2] = {SERVER_SET_PLAYER, playerIndex};
    nrf24_SendMsg(address, BOMBERMAN_MSG, data, 2);
}

void serverMessageRequestPlayers()
{
    uint8_t data[1] = {SERVER_REQUEST_PLAYERS};


    nrf24_SendMsg(BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    1);
}

void serverMessageGameStart(uint8_t numberOfPlayers)
{

    uint8_t data[2] = {SERVER_GAME_START, numberOfPlayers};

    nrf24_SendMsg(BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    2);
}

void serverMessagePlaceBomb( uint8_t x, uint8_t y)
{
    uint8_t data[3] = {SERVER_PLACE_BOMB, x, y};

    nrf24_SendMsg(  BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    3);
}

void serverMessageExplodeBomb( uint8_t x, uint8_t y, uint8_t blastSize)
{
    uint8_t data[4] = {SERVER_EXPLODE_BOMB, x, y, blastSize};

    nrf24_SendMsg(  BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    4);
}

void serverMessageMovePlayer( uint8_t playerIndex, uint8_t direction)
{
    uint8_t data[3] = {SERVER_MOVE_PLAYER, playerIndex, direction};

    nrf24_SendMsg(  BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    3);
}

void serverMessageKillPlayer( uint8_t playerIndex)
{
    uint8_t data[2] = {SERVER_KILL_PLAYER, playerIndex};

    nrf24_SendMsg(  BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    2);
}

void serverMessageDestroyBlock( uint8_t x, uint8_t y)
{
    uint8_t data[3] = {SERVER_DESTROY_BLOCK, x, y};

    nrf24_SendMsg(  BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    3);
}

void serverMessagePlacePowerup( uint8_t type, uint8_t x, uint8_t y)
{
    uint8_t data[4] = {SERVER_PLACE_POWERUP, type, x, y};

    nrf24_SendMsg(  BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    4);
}

void serverMessagePowerupPlayer(uint8_t address, uint8_t type)
{
    uint8_t data[2] = {SERVER_POWERUP_PLAYER, type};

    nrf24_SendMsg(  address,
                    BOMBERMAN_MSG,
                    data,
                    2);
}

void serverMessageGameOver(uint8_t playerIndex)
{
    uint8_t data[2] = {SERVER_GAME_OVER, playerIndex};

    nrf24_SendMsg(  BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    2);
}

void serverMessageRemovePowerup(uint8_t x, uint8_t y)
{
    uint8_t data[3] = {SERVER_REMOVE_POWERUP, x, y};
    nrf24_SendMsg(  BOMBERMAN_CLIENT_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    3);
}

void clientMessageRegisterPlayer()
{
    uint8_t data[2] = {CLIENT_REGISTER_PLAYER};
    nrf24_SendMsg(  BOMBERMAN_SERVER_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    2);
}

void clientMessagePlaceBomb()
{

    uint8_t data[2] = {CLIENT_PLACE_BOMB, gameState.playerIndex};

    nrf24_SendMsg(  BOMBERMAN_SERVER_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    2);
}

void clientMessageMove( uint8_t direction)
{
    uint8_t data[3] = {CLIENT_MOVE, gameState.playerIndex, direction};
    nrf24_SendMsg(  BOMBERMAN_SERVER_ADDRESS,
                    BOMBERMAN_MSG,
                    data,
                    3);
}

void serverHandleGameOver(uint8_t winnerPlayerIndex)
{
    gameState.gameMode = GAMESTATE_GAME_OVER;
    serverMessageGameOver(winnerPlayerIndex);
    Game_CharXY(' ', 0, 10);
    Game_Printf("Game Over!\n\r");
    Game_Printf(getName(players[winnerPlayerIndex].address));
    Game_Printf(" Won!!\n\r");
    Game_SetColor( ForegroundWhite);
    Game_SetColor( BackgroundBlack);
    Game_UnregisterPlayer1Receiver(handleServerInput);
    Game_ShowCursor();
}

void clientHandleGameOver(uint8_t winnerPlayerIndex)
{
    Game_CharXY(' ', 0, 10);
    Game_Printf("Game Over!\n\r");

    if(gameState.playerIndex == winnerPlayerIndex)
    {
        Game_SetColor( BackgroundWhite);
        Game_SetColor( ForegroundBlue);
        Game_Player1Printf("You Won!\n\r");
    }
    else
    {
        Game_SetColor( BackgroundRed);
        Game_SetColor( ForegroundYellow);
        Game_Player1Printf("You Lost!\n\r");
    }

    Game_SetColor( ForegroundWhite);
    Game_SetColor( BackgroundBlack);
    Game_UnregisterPlayer1Receiver(handleClientInput);
    Game_ShowCursor();
}


void e_clientMessageRegisterPlayer(uint8_t playerAddress)
{
    uint8_t data[1] = {CLIENT_REGISTER_PLAYER};
    serverMessageHandler(data, 1, playerAddress);
}

void e_clientMessagePlaceBomb(uint8_t playerAddress, uint8_t playerIndex)
{
    uint8_t data[2] = {CLIENT_PLACE_BOMB, playerIndex};
    serverMessageHandler(data, 2, playerAddress);
}

void e_clientMessageMove( uint8_t playerAddress, uint8_t playerIndex, uint8_t direction)
{
    uint8_t data[3] = {CLIENT_MOVE, playerIndex, direction};
    serverMessageHandler(data, 3, playerAddress);
}

void e_serverMessageRequestPlayers()
{
    uint8_t data[1] = {SERVER_REQUEST_PLAYERS};
    clientMessageHandler(data, 1, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessageSetPlayer(uint8_t playerIndex)
{
    uint8_t data[2] = {SERVER_SET_PLAYER, 0};
    clientMessageHandler(data, 2, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessageGameStart(uint8_t numberOfPlayers)
{
    uint8_t data[2] = {SERVER_GAME_START, 1};
    clientMessageHandler(data, 1, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessagePlaceBomb(uint8_t x, uint8_t y)
{
    uint8_t data[3] = {SERVER_PLACE_BOMB, x, y};
    clientMessageHandler(data, 3, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessageExplodeBomb(uint8_t x, uint8_t y, uint8_t blastSize)
{
    uint8_t data[4] = {SERVER_EXPLODE_BOMB, x, y, blastSize};
    clientMessageHandler(data, 4, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessageMovePlayer(uint8_t playerIndex, uint8_t direction)
{
    uint8_t data[3] = {SERVER_MOVE_PLAYER, playerIndex, direction};
    clientMessageHandler(data, 3, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessageKillPlayer(uint8_t playerIndex)
{
    uint8_t data[2] = {SERVER_KILL_PLAYER, playerIndex};
    clientMessageHandler(data, 2, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessageDestroyBlock(uint8_t x, uint8_t y)
{
    uint8_t data[3] = {SERVER_DESTROY_BLOCK, x, y};
    clientMessageHandler(data, 3, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessagePlacePowerup(uint8_t x, uint8_t y, uint8_t type)
{
    uint8_t data[4] = {SERVER_PLACE_POWERUP, x, y, type};
    clientMessageHandler(data, 4, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessageRemovePowerup(uint8_t x, uint8_t y)
{
    uint8_t data[3] = {SERVER_REMOVE_POWERUP, x, y};
    clientMessageHandler(data, 3, BOMBERMAN_SERVER_ADDRESS);
}

void e_serverMessageGameOver(uint8_t playerIndex)
{
    uint8_t data[2] = {SERVER_GAME_OVER, playerIndex};
    clientMessageHandler(data,2, BOMBERMAN_SERVER_ADDRESS);
}

void e_addNewPlayer()
{
    static uint8_t i = 0;
    //Each time this function is called it adds the next player
    serverHandleRegisterPlayer(BOMBERMAN_SERVER_ADDRESS + i);
    i++;
}

void e_moveClient1(char key)
{
    switch(key)
    {
        case '1':
            e_clientMessageMove(BOMBERMAN_SERVER_ADDRESS, 0, MOVE_UP);
            break;
        case '2':
            e_clientMessageMove(BOMBERMAN_SERVER_ADDRESS, 0, MOVE_DOWN);
            break;
        case '3':
            e_clientMessageMove(BOMBERMAN_SERVER_ADDRESS, 0, MOVE_LEFT);
            break;
        case '4':
            e_clientMessageMove(BOMBERMAN_SERVER_ADDRESS, 0, MOVE_RIGHT);
            break;

    }
}

void e_moveClient2(char key)
{
    switch(key)
    {
        case '5':
            e_clientMessageMove(ALEYAN, 1, MOVE_UP);
            break;
        case '6':
            e_clientMessageMove(ALEYAN, 1, MOVE_DOWN);
            break;
        case '7':
            e_clientMessageMove(ALEYAN, 1, MOVE_LEFT);
            break;
        case '8':
            e_clientMessageMove(ALEYAN, 1, MOVE_RIGHT);
            break;

    }
}

void e_placeBombPlayer1()
{
    e_clientMessagePlaceBomb(BOMBERMAN_SERVER_ADDRESS, 0);
}
void e_placeBombPlayer2()
{
    e_clientMessagePlaceBomb(ALEYAN, 1);
}

void serverSpeedTest()
{
    uint8_t data[1] = {SPEED_TEST};
    LogMsg(0, "Speed Test");
    nrf24_SendMsg(BOMBERMAN_CLIENT_ADDRESS,BOMBERMAN_MSG, data, 1);
}

void clientSpeedTest()
{
    uint8_t data[1] = {SPEED_TEST};
    LogMsg(0, "Speed Test");
    nrf24_SendMsg(BOMBERMAN_SERVER_ADDRESS,BOMBERMAN_MSG, data, 1);
}