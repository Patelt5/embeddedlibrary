#ifndef _BIG_INT_H_
#define _BIG_INT_H_

/** @file
 * @defgroup big_int Haas big integer module
 * @addtogroup JJ_chat
 *
 * @brief Types and functions for arbitrary-precision integer math
 *
 * This module implements a @c big_int_t struct and functions that can
 * be used for arbitrary-precision integer math. The struct keeps track
 * of an array of bytes (type @c uint8_t) and the array's size. Note that
 * the least significant byte is at position 0 and the most significant
 * byte is at the end of the array (effectively Little Endian).
 *
 * This implementation is not guaranteed to be fast or optimized. This
 * module was written specifically for encryption (using RSA), and so
 * might be missing some standard math functions.
 *
 * Supported math operations: negation, comparison, addition, subtraction,
 * multiplication, modulus, modular exponentiation
 *
 * Example code to implement "123+234=357" which is the same as "7B+EA=165":
 *
 * @code
 * #include "big_int.h"
 *
 * int main() {
 *     big_int_t a,b,c;
 *     big_init(&a,1);
 *     big_init(&b,1);
 *
 *     a.bytes[0] = 123;
 *     b.bytes[0] = 234;
 *     big_add(&a,&b,&c);

 *     big_print(&a);
 *     big_print(&b);
 *     big_print(&c);

 *     big_kill(&a);
 *     big_kill(&b);
 *     big_kill(&c);
 *
 *     return 0;
 * }
 * @endcode
 *
 * @warning Most functions in this module use uint8_t to keep track of
 * size. Therefore a big_int_t must not exceed 256 bytes.
 * @warning Always call big_kill() when done to free() the malloc().
 *
 * @author Joshua Haas
 */





#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/**
 * Constant definitions for comparison function.
 */
#define GRT_A 1
#define GRT_B 0
#define EQUAL 2

/**
 * Definition of the big_int_t type struct
 */
typedef struct {
  uint8_t size;
  uint8_t *bytes;
  uint8_t neg;
} big_int_t;



/**
 * @brief Initialize a big_int_t
 *
 * This function initializes the fields in the struct ans uses malloc()
 * to assign memory for the byte array.
 *
 * @param *big pointer to an uninitialized big_int_t
 * @param size number of bytes to initialize
 *
 * @warning This function must be called before any others can be.
 */
void big_init(big_int_t *big, uint8_t size);

/**
 * @brief Set the value of a big_int_t
 *
 * Parse the hex string and use it to set the value of the big_int_t.
 *
 * @param *big pointer to an initialized big_int_t
 * @param *hex string literal or pointer to a char array
 * @param size number of characters in hex
 */
void big_set(big_int_t *big, char *hex, uint8_t size);

/**
 * @brief Copy the value of a into b
 *
 * Copies the values in the byte array of a into that of b.
 *
 * @param a pointer to an initialized big_int_t
 * @param b pointer to an initialized big_int_t
 *
 * @warning a must be the same size or smaller than b
 */
void big_copy(big_int_t *a, big_int_t *b);

/**
 * @brief Print the big_num_t as hex values
 *
 * Prints the given big_num_t using hex values. Note that although the
 * internal representation is Little Endian, this function prints it as
 * Big Endian since that is the format most people are used to.
 *
 * @param *big pointer to an initialized big_int_t
 */
void big_print(big_int_t *big);

/**
 * @brief Convert 4 bits to a hex char
 *
 * This function accepts a uint8_t but only cares about the 4 least
 * significant bits. Any values outside 0-15 will return '0'.
 *
 * @param num the number to convert to hex
 *
 * @return the hex character
 *
 * @warning Bit mask the input if necessary (e.g. x & 0x0F)
 */
char to_hex(uint8_t num);

/**
 * @brief Change the size of the given big_int_t
 *
 * This function uses realloc() to change the size of a big_int_t. It
 * can grow or shrink the input. Shrinking removes the most significant
 * bytes. Growing adds zeros as the most significant bytes.
 *
 * @param *big pointer to an initialized big_int_t
 * @param size new size for the big_int_t
 */
void big_resize(big_int_t *big, uint8_t size);

/**
 * @brief Normalize the given big_int_t
 *
 * This function removes leading zeros from the given big_int_t and
 * shrinks it to the minimum size possible (at least 1 byte).
 *
 * @param *big pointer to an initialized big_int_t
 */
void big_norm(big_int_t *big);

/**
 * @brief Delete the given big_int_t
 *
 * This function uses free() to unallocate the memory used for the byte
 * array originally allocated in big_init().
 *
 * @param *big pointer to an initialized big_int_t
 *
 * @warning Always call this function on every big_int_t when done with them.
 */
void big_kill(big_int_t *big);





/**
 * @brief Negate the given big_int_t
 *
 * This just flips the bit in the "neg" property of the struct, but is
 * included in case the module is ever re-written to use two's complement.
 *
 * @param *big pointer to an initialized big_int_t
 */
void big_neg(big_int_t *big);

/**
 * @brief Compare the values of two big_int_t
 *
 * This function compares the absolute values of two big_int_t and returns
 * the result using the macros defined in this header. Based on these
 * definitions, testing @c if(big_cmp(a,b)) is equivalent to
 * @c if(a>=b), and testing @c if(!big_cmp(a,b)) is equivalent to
 * @c if(a<b). Use of the macros is recommended.
 *
 * - GRT_A = 1 : First parameter is greater
 * - GRT_B = 0 : Second parameter is greater
 * - EQUAL = 2 : The parameters are equal
 *
 * @param *a pointer to an initialized big_int_t
 * @param *b pointer to an initialized big_int_t
 *
 * @return the result of the comparison
 *
 * @warning This function ignores negative numbers and acts on the absolute value.
 */
uint8_t big_cmp(big_int_t *a, big_int_t *b);

/**
 * @brief Add the two big_int_t
 *
 * This function adds the two big_int_t and stores the result in the
 * pointer ans. The two parameters can be positive or negative. The
 * result could be larger than the larger big_num_t by 1-byte, or could
 * be a minimum of 1 byte in the case of a negative input.
 *
 * @param *a pointer to an initialized big_int_t
 * @param *b pointer to an initialized big_int_t
 * @param *ans pointer to a big_int_t (does not have to be initialized)
 */
void big_add(big_int_t *a, big_int_t *b, big_int_t *ans);

/**
 * @brief Subtract the two big_int_t
 *
 * This function subtracts the two big_int_t and stores the result in the
 * pointer ans. This is a wrapper function for big_add() provided for
 * convenience.
 *
 * @param *a pointer to an initialized big_int_t
 * @param *b pointer to an initialized big_int_t
 * @param *ans pointer to a big_int_t (does not have to be initialized)
 */
void big_sub(big_int_t *a, big_int_t *b, big_int_t *ans);

/**
 * @brief Multiply the two big_int_t
 *
 * This function multiplies the two big_int_t and stores the result in the
 * pointer ans. The two parameters can be positive or negative. The
 * result could be up to the sum of the two inputs' sizes long, or
 * could be a minimum of 1 byte.
 *
 * @param *a pointer to an initialized big_int_t
 * @param *b pointer to an initialized big_int_t
 * @param *ans pointer to a big_int_t (does not have to be initialized)
 */
void big_mult(big_int_t *a, big_int_t *b, big_int_t *ans);

/**
 * @brief Take the modulo of the two inputs
 *
 * This function calculates the modulo big%mod of the inputs.
 *
 * @param *a pointer to an initialized big_int_t
 * @param mod the modulus to use
 *
 * @return the remainder of the modulo operation
 *
 * @warning Negative is ignored in this operation.
 */
uint8_t big_mod(big_int_t *big, uint8_t mod);

/**
 * @brief Calculate the modular exponentiation of the inputs
 *
 * This function calculates the modular exponentiation of the inputs, i.e.
 * (big^exp)%mod and stores it in the pointer ans. This function is
 * used for encrypting and decrypting with the RSA algorithm.
 *
 * @param *big pointer to an initialized big_int_t
 * @param exp the power to which to raise the big_int_t
 * @param *ans pointer to a big_int_t (does not have to be initialized)
 */
void big_powmod(big_int_t *big, uint8_t exp, uint8_t mod, big_int_t *ans);

#endif /* _BIG_INT_H_ */