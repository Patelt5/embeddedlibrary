
 /**
 * @defgroup hal_uart HAL UART Module
 * @ingroup uart
 * @file hal_uart.h
 *
 *  Created on: Feb 8, 2015
 *      Author: Anthony Merlino
 * @todo Anthony Merlino document this
 * @{
 */

#ifndef _HAL_UART_H_
#define _HAL_UART_H_

/**
 * UART Channel 0
 *
 * TX: P3.3 (HW Pin 40)
 * RX: P3.4 (HW Pin 41)
 */
#define UART0 0

/**
 * UART Channel 1
 *
 * TX: P4.4 (HW Pin 51)
 * RX: P4.5 (HW Pin 52)
 */
#define UART1 1

/** @}*/
#endif /* _HAL_UART_H_ */
